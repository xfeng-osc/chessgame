package com.sojoys.chess.game.db.cfg;

import java.util.Map;
import com.sojoys.artifact.constant.DataModelKeyEnum;
import com.sojoys.artifact.build.data.base.BaseCfg;
import com.sojoys.artifact.tools.ToolMap;
/**
 * playing_type
 */
 public class Playing_typeCfg extends BaseCfg<Playing_typeCfg>{
	/**
	 * 数据库表名称
	 */
	public static final String TABLE_NAME = "playing_type";
	/**
	 * 完整类名称
	 */
	public static final String CLASS_NAME = "com.sojoys.chess.game.db.cfg.Playing_typeCfg"; 
	/**
	 * 字段个数
	 */
	public static final int COLUMN_COUNT= 2;
	/**
	 * 数据库源字段名字
	 */
	public static final String[] SOURCE_COLUMN_NAMES = new String[]{"id","name",};
	/**
	 * 拼音字段名字
	 */
	public static final String[] PINGYING_COLUMN_NAMES = new String[]{"id","name",};
	/**
	 * HashCode字段名字
	 */
	public static final Integer[] HASHCODE_COLUMN_NAMES = new Integer[]{3355,3373707,};
	/**
	 * 字段基本类型
	 */
	public static final String[] COLUMN_BASICTYPES = new String[]{"int","String",};
	/**
	 * 字段引用类型
	 */
	public static final Class<?>[] COLUMN_CLASSTYPES = new Class[]{Integer.class,String.class,};
	
	/** id */
	private int id;
	
	/** name */
	private String name;
	
	
	
	public static Playing_typeCfg builder() {
		Playing_typeCfg playing_typeCfg = new Playing_typeCfg();
		return playing_typeCfg;
	}
	
	/** id */
	public int getId() {
		return id;
	}
	
	/** name */
	public String getName() {
		return name;
	}
	
	
	protected Object[] getColumnNames(DataModelKeyEnum e){
		switch (e) {
		case SOURCE:
			return SOURCE_COLUMN_NAMES;
		case PINGYING:
			return PINGYING_COLUMN_NAMES;
		case HASHCODE:
			return HASHCODE_COLUMN_NAMES;
		default:
			return PINGYING_COLUMN_NAMES;
		}
	}
	
	protected String[] getBasicTypes(){
		return COLUMN_BASICTYPES;
	}
	
	protected Class<?>[] getClassTypes(){
		return COLUMN_CLASSTYPES;
	}
	
	protected Object[] getColumeValues(){
		Object[] values = new Object[COLUMN_COUNT];
        values[0] = this.id;
        values[1] = this.name;
        return values;
	}
	
	protected Playing_typeCfg createColumeValues(Object[] vals) {
		this.id = (Integer)vals[0];
		this.name = (String)vals[1];
		return this;
	}
	
	protected Map<Object, Object> toMap(Object[] keys,Map<Object, Object> map,boolean encode){
        map.put(keys[0], this.id);
        map.put(keys[1], this.name);
        return map;
    }
	
	protected Playing_typeCfg createForMap(Object[] keys,Map<?, ?> map,boolean decode){
	    this.id = ToolMap.getInt(keys[0], map);
	    this.name = ToolMap.getString(keys[1], map);
        return this;
    }
	
	public Playing_typeCfg deepCopy(){
		Playing_typeCfg playing_typeCfg = null;
		try {
			playing_typeCfg = (Playing_typeCfg) super.clone();
			return playing_typeCfg;
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return playing_typeCfg;
	}
}
