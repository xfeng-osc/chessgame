package com.sojoys.chess.game.netty;

import com.sojoys.artifact.plugin.netty.coder.NettyMessageDecoder;
import com.sojoys.artifact.plugin.netty.coder.NettyMessageEncoder;
import com.sojoys.artifact.plugin.netty.message.NettyMessage;

import io.netty.channel.CombinedChannelDuplexHandler;
import io.netty.handler.codec.ByteToMessageDecoder;
import io.netty.handler.codec.MessageToByteEncoder;

/**
 * 自定义加密方式的服务器编码解码处理器
 * @author wk.dai
 */
public class NettyCodec
		extends
		CombinedChannelDuplexHandler<ByteToMessageDecoder, MessageToByteEncoder<NettyMessage>> {

	public NettyCodec() {
		this(new NettyMessageDecoder(), new NettyMessageEncoder());
	}

	public NettyCodec(ByteToMessageDecoder inboundHandler,
			MessageToByteEncoder<NettyMessage> outboundHandler) {
		super(inboundHandler, outboundHandler);
	}
}
