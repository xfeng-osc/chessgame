package com.sojoys.chess.game.db;

import com.sojoys.chess.game.db.cfg.cache.*;
import com.sojoys.artifact.tools.SK_Plus;
import java.io.File;
import java.io.IOException;
import jxl.Workbook;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
/**
 * 加载所有的配置
 */
public class ConfigLoad {
	public static boolean loadAllConfig(String path) throws Exception{
		// constant
		ConstantCfgCache.getInstance().loadFile(SK_Plus.b(path, "Constant.sk").e());
		
		// desk
		DeskCfgCache.getInstance().loadFile(SK_Plus.b(path, "Desk.sk").e());
		
		// playing_type
		Playing_typeCfgCache.getInstance().loadFile(SK_Plus.b(path, "Playing_type.sk").e());
		
		return true;
	}
	
	public static boolean writeAllConfig(String path) throws Exception{
		// constant
		ConstantCfgCache.getInstance().writeFile(SK_Plus.b(path, "Constant.sk").e());
		
		// desk
		DeskCfgCache.getInstance().writeFile(SK_Plus.b(path, "Desk.sk").e());
		
		// playing_type
		Playing_typeCfgCache.getInstance().writeFile(SK_Plus.b(path, "Playing_type.sk").e());
		
		return true;
	}
	
	public static boolean writeAllExcel(String path) throws Exception{
		WritableWorkbook wwb = null;
		try {
			wwb = Workbook.createWorkbook(new File(path));
			// constant
			ConstantCfgCache.getInstance().writeExcel(wwb,0);
			// desk
			DeskCfgCache.getInstance().writeExcel(wwb,1);
			// playing_type
			Playing_typeCfgCache.getInstance().writeExcel(wwb,2);
			wwb.write();//将内容写到excel文件中
		} catch (IOException e) {
			e.printStackTrace();
		} catch (WriteException e) {
			e.printStackTrace();
		} finally {
		   // 关闭 Excel 工作薄对象
			try {
				if(wwb!=null){
					wwb.close();
				}
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	public static boolean loadAllExcel(String path) throws Exception{
		Workbook book = Workbook.getWorkbook(new File(path));
		// constant
		ConstantCfgCache.getInstance().loadExcel(book);
		// desk
		DeskCfgCache.getInstance().loadExcel(book);
		// playing_type
		Playing_typeCfgCache.getInstance().loadExcel(book);
		book.close();
		return true;
	}
//自定义内容起始位置
//自定义内容结束位置
}