// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: request/Req_LoginMsg.proto

package com.sojoys.chess.game.protobuf.request;

public final class Req_LoginMsg {
  private Req_LoginMsg() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
  }
  public interface EnterGameOrBuilder
      extends com.google.protobuf.MessageOrBuilder {

    // required string token = 1;
    /**
     * <code>required string token = 1;</code>
     *
     * <pre>
     *token
     * </pre>
     */
    boolean hasToken();
    /**
     * <code>required string token = 1;</code>
     *
     * <pre>
     *token
     * </pre>
     */
    java.lang.String getToken();
    /**
     * <code>required string token = 1;</code>
     *
     * <pre>
     *token
     * </pre>
     */
    com.google.protobuf.ByteString
        getTokenBytes();
  }
  /**
   * Protobuf type {@code com.sojoys.chess.game.protobuf.request.EnterGame}
   *
   * <pre>
   *游戏服务器注册请求 -&gt; cmd=257  return=Res_PlayerMsg.Player
   * </pre>
   */
  public static final class EnterGame extends
      com.google.protobuf.GeneratedMessage
      implements EnterGameOrBuilder {
    // Use EnterGame.newBuilder() to construct.
    private EnterGame(com.google.protobuf.GeneratedMessage.Builder<?> builder) {
      super(builder);
      this.unknownFields = builder.getUnknownFields();
    }
    private EnterGame(boolean noInit) { this.unknownFields = com.google.protobuf.UnknownFieldSet.getDefaultInstance(); }

    private static final EnterGame defaultInstance;
    public static EnterGame getDefaultInstance() {
      return defaultInstance;
    }

    public EnterGame getDefaultInstanceForType() {
      return defaultInstance;
    }

    private final com.google.protobuf.UnknownFieldSet unknownFields;
    @java.lang.Override
    public final com.google.protobuf.UnknownFieldSet
        getUnknownFields() {
      return this.unknownFields;
    }
    private EnterGame(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      initFields();
      int mutable_bitField0_ = 0;
      com.google.protobuf.UnknownFieldSet.Builder unknownFields =
          com.google.protobuf.UnknownFieldSet.newBuilder();
      try {
        boolean done = false;
        while (!done) {
          int tag = input.readTag();
          switch (tag) {
            case 0:
              done = true;
              break;
            default: {
              if (!parseUnknownField(input, unknownFields,
                                     extensionRegistry, tag)) {
                done = true;
              }
              break;
            }
            case 10: {
              bitField0_ |= 0x00000001;
              token_ = input.readBytes();
              break;
            }
          }
        }
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.setUnfinishedMessage(this);
      } catch (java.io.IOException e) {
        throw new com.google.protobuf.InvalidProtocolBufferException(
            e.getMessage()).setUnfinishedMessage(this);
      } finally {
        this.unknownFields = unknownFields.build();
        makeExtensionsImmutable();
      }
    }
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return com.sojoys.chess.game.protobuf.request.Req_LoginMsg.internal_static_com_sojoys_chess_game_protobuf_request_EnterGame_descriptor;
    }

    protected com.google.protobuf.GeneratedMessage.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return com.sojoys.chess.game.protobuf.request.Req_LoginMsg.internal_static_com_sojoys_chess_game_protobuf_request_EnterGame_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              com.sojoys.chess.game.protobuf.request.Req_LoginMsg.EnterGame.class, com.sojoys.chess.game.protobuf.request.Req_LoginMsg.EnterGame.Builder.class);
    }

    public static com.google.protobuf.Parser<EnterGame> PARSER =
        new com.google.protobuf.AbstractParser<EnterGame>() {
      public EnterGame parsePartialFrom(
          com.google.protobuf.CodedInputStream input,
          com.google.protobuf.ExtensionRegistryLite extensionRegistry)
          throws com.google.protobuf.InvalidProtocolBufferException {
        return new EnterGame(input, extensionRegistry);
      }
    };

    @java.lang.Override
    public com.google.protobuf.Parser<EnterGame> getParserForType() {
      return PARSER;
    }

    private int bitField0_;
    // required string token = 1;
    public static final int TOKEN_FIELD_NUMBER = 1;
    private java.lang.Object token_;
    /**
     * <code>required string token = 1;</code>
     *
     * <pre>
     *token
     * </pre>
     */
    public boolean hasToken() {
      return ((bitField0_ & 0x00000001) == 0x00000001);
    }
    /**
     * <code>required string token = 1;</code>
     *
     * <pre>
     *token
     * </pre>
     */
    public java.lang.String getToken() {
      java.lang.Object ref = token_;
      if (ref instanceof java.lang.String) {
        return (java.lang.String) ref;
      } else {
        com.google.protobuf.ByteString bs = 
            (com.google.protobuf.ByteString) ref;
        java.lang.String s = bs.toStringUtf8();
        if (bs.isValidUtf8()) {
          token_ = s;
        }
        return s;
      }
    }
    /**
     * <code>required string token = 1;</code>
     *
     * <pre>
     *token
     * </pre>
     */
    public com.google.protobuf.ByteString
        getTokenBytes() {
      java.lang.Object ref = token_;
      if (ref instanceof java.lang.String) {
        com.google.protobuf.ByteString b = 
            com.google.protobuf.ByteString.copyFromUtf8(
                (java.lang.String) ref);
        token_ = b;
        return b;
      } else {
        return (com.google.protobuf.ByteString) ref;
      }
    }

    private void initFields() {
      token_ = "";
    }
    private byte memoizedIsInitialized = -1;
    public final boolean isInitialized() {
      byte isInitialized = memoizedIsInitialized;
      if (isInitialized != -1) return isInitialized == 1;

      if (!hasToken()) {
        memoizedIsInitialized = 0;
        return false;
      }
      memoizedIsInitialized = 1;
      return true;
    }

    public void writeTo(com.google.protobuf.CodedOutputStream output)
                        throws java.io.IOException {
      getSerializedSize();
      if (((bitField0_ & 0x00000001) == 0x00000001)) {
        output.writeBytes(1, getTokenBytes());
      }
      getUnknownFields().writeTo(output);
    }

    private int memoizedSerializedSize = -1;
    public int getSerializedSize() {
      int size = memoizedSerializedSize;
      if (size != -1) return size;

      size = 0;
      if (((bitField0_ & 0x00000001) == 0x00000001)) {
        size += com.google.protobuf.CodedOutputStream
          .computeBytesSize(1, getTokenBytes());
      }
      size += getUnknownFields().getSerializedSize();
      memoizedSerializedSize = size;
      return size;
    }

    private static final long serialVersionUID = 0L;
    @java.lang.Override
    protected java.lang.Object writeReplace()
        throws java.io.ObjectStreamException {
      return super.writeReplace();
    }

    public static com.sojoys.chess.game.protobuf.request.Req_LoginMsg.EnterGame parseFrom(
        com.google.protobuf.ByteString data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static com.sojoys.chess.game.protobuf.request.Req_LoginMsg.EnterGame parseFrom(
        com.google.protobuf.ByteString data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static com.sojoys.chess.game.protobuf.request.Req_LoginMsg.EnterGame parseFrom(byte[] data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static com.sojoys.chess.game.protobuf.request.Req_LoginMsg.EnterGame parseFrom(
        byte[] data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static com.sojoys.chess.game.protobuf.request.Req_LoginMsg.EnterGame parseFrom(java.io.InputStream input)
        throws java.io.IOException {
      return PARSER.parseFrom(input);
    }
    public static com.sojoys.chess.game.protobuf.request.Req_LoginMsg.EnterGame parseFrom(
        java.io.InputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return PARSER.parseFrom(input, extensionRegistry);
    }
    public static com.sojoys.chess.game.protobuf.request.Req_LoginMsg.EnterGame parseDelimitedFrom(java.io.InputStream input)
        throws java.io.IOException {
      return PARSER.parseDelimitedFrom(input);
    }
    public static com.sojoys.chess.game.protobuf.request.Req_LoginMsg.EnterGame parseDelimitedFrom(
        java.io.InputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return PARSER.parseDelimitedFrom(input, extensionRegistry);
    }
    public static com.sojoys.chess.game.protobuf.request.Req_LoginMsg.EnterGame parseFrom(
        com.google.protobuf.CodedInputStream input)
        throws java.io.IOException {
      return PARSER.parseFrom(input);
    }
    public static com.sojoys.chess.game.protobuf.request.Req_LoginMsg.EnterGame parseFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return PARSER.parseFrom(input, extensionRegistry);
    }

    public static Builder newBuilder() { return Builder.create(); }
    public Builder newBuilderForType() { return newBuilder(); }
    public static Builder newBuilder(com.sojoys.chess.game.protobuf.request.Req_LoginMsg.EnterGame prototype) {
      return newBuilder().mergeFrom(prototype);
    }
    public Builder toBuilder() { return newBuilder(this); }

    @java.lang.Override
    protected Builder newBuilderForType(
        com.google.protobuf.GeneratedMessage.BuilderParent parent) {
      Builder builder = new Builder(parent);
      return builder;
    }
    /**
     * Protobuf type {@code com.sojoys.chess.game.protobuf.request.EnterGame}
     *
     * <pre>
     *游戏服务器注册请求 -&gt; cmd=257  return=Res_PlayerMsg.Player
     * </pre>
     */
    public static final class Builder extends
        com.google.protobuf.GeneratedMessage.Builder<Builder>
       implements com.sojoys.chess.game.protobuf.request.Req_LoginMsg.EnterGameOrBuilder {
      public static final com.google.protobuf.Descriptors.Descriptor
          getDescriptor() {
        return com.sojoys.chess.game.protobuf.request.Req_LoginMsg.internal_static_com_sojoys_chess_game_protobuf_request_EnterGame_descriptor;
      }

      protected com.google.protobuf.GeneratedMessage.FieldAccessorTable
          internalGetFieldAccessorTable() {
        return com.sojoys.chess.game.protobuf.request.Req_LoginMsg.internal_static_com_sojoys_chess_game_protobuf_request_EnterGame_fieldAccessorTable
            .ensureFieldAccessorsInitialized(
                com.sojoys.chess.game.protobuf.request.Req_LoginMsg.EnterGame.class, com.sojoys.chess.game.protobuf.request.Req_LoginMsg.EnterGame.Builder.class);
      }

      // Construct using com.sojoys.chess.game.protobuf.request.Req_LoginMsg.EnterGame.newBuilder()
      private Builder() {
        maybeForceBuilderInitialization();
      }

      private Builder(
          com.google.protobuf.GeneratedMessage.BuilderParent parent) {
        super(parent);
        maybeForceBuilderInitialization();
      }
      private void maybeForceBuilderInitialization() {
        if (com.google.protobuf.GeneratedMessage.alwaysUseFieldBuilders) {
        }
      }
      private static Builder create() {
        return new Builder();
      }

      public Builder clear() {
        super.clear();
        token_ = "";
        bitField0_ = (bitField0_ & ~0x00000001);
        return this;
      }

      public Builder clone() {
        return create().mergeFrom(buildPartial());
      }

      public com.google.protobuf.Descriptors.Descriptor
          getDescriptorForType() {
        return com.sojoys.chess.game.protobuf.request.Req_LoginMsg.internal_static_com_sojoys_chess_game_protobuf_request_EnterGame_descriptor;
      }

      public com.sojoys.chess.game.protobuf.request.Req_LoginMsg.EnterGame getDefaultInstanceForType() {
        return com.sojoys.chess.game.protobuf.request.Req_LoginMsg.EnterGame.getDefaultInstance();
      }

      public com.sojoys.chess.game.protobuf.request.Req_LoginMsg.EnterGame build() {
        com.sojoys.chess.game.protobuf.request.Req_LoginMsg.EnterGame result = buildPartial();
        if (!result.isInitialized()) {
          throw newUninitializedMessageException(result);
        }
        return result;
      }

      public com.sojoys.chess.game.protobuf.request.Req_LoginMsg.EnterGame buildPartial() {
        com.sojoys.chess.game.protobuf.request.Req_LoginMsg.EnterGame result = new com.sojoys.chess.game.protobuf.request.Req_LoginMsg.EnterGame(this);
        int from_bitField0_ = bitField0_;
        int to_bitField0_ = 0;
        if (((from_bitField0_ & 0x00000001) == 0x00000001)) {
          to_bitField0_ |= 0x00000001;
        }
        result.token_ = token_;
        result.bitField0_ = to_bitField0_;
        onBuilt();
        return result;
      }

      public Builder mergeFrom(com.google.protobuf.Message other) {
        if (other instanceof com.sojoys.chess.game.protobuf.request.Req_LoginMsg.EnterGame) {
          return mergeFrom((com.sojoys.chess.game.protobuf.request.Req_LoginMsg.EnterGame)other);
        } else {
          super.mergeFrom(other);
          return this;
        }
      }

      public Builder mergeFrom(com.sojoys.chess.game.protobuf.request.Req_LoginMsg.EnterGame other) {
        if (other == com.sojoys.chess.game.protobuf.request.Req_LoginMsg.EnterGame.getDefaultInstance()) return this;
        if (other.hasToken()) {
          bitField0_ |= 0x00000001;
          token_ = other.token_;
          onChanged();
        }
        this.mergeUnknownFields(other.getUnknownFields());
        return this;
      }

      public final boolean isInitialized() {
        if (!hasToken()) {
          
          return false;
        }
        return true;
      }

      public Builder mergeFrom(
          com.google.protobuf.CodedInputStream input,
          com.google.protobuf.ExtensionRegistryLite extensionRegistry)
          throws java.io.IOException {
        com.sojoys.chess.game.protobuf.request.Req_LoginMsg.EnterGame parsedMessage = null;
        try {
          parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
        } catch (com.google.protobuf.InvalidProtocolBufferException e) {
          parsedMessage = (com.sojoys.chess.game.protobuf.request.Req_LoginMsg.EnterGame) e.getUnfinishedMessage();
          throw e;
        } finally {
          if (parsedMessage != null) {
            mergeFrom(parsedMessage);
          }
        }
        return this;
      }
      private int bitField0_;

      // required string token = 1;
      private java.lang.Object token_ = "";
      /**
       * <code>required string token = 1;</code>
       *
       * <pre>
       *token
       * </pre>
       */
      public boolean hasToken() {
        return ((bitField0_ & 0x00000001) == 0x00000001);
      }
      /**
       * <code>required string token = 1;</code>
       *
       * <pre>
       *token
       * </pre>
       */
      public java.lang.String getToken() {
        java.lang.Object ref = token_;
        if (!(ref instanceof java.lang.String)) {
          java.lang.String s = ((com.google.protobuf.ByteString) ref)
              .toStringUtf8();
          token_ = s;
          return s;
        } else {
          return (java.lang.String) ref;
        }
      }
      /**
       * <code>required string token = 1;</code>
       *
       * <pre>
       *token
       * </pre>
       */
      public com.google.protobuf.ByteString
          getTokenBytes() {
        java.lang.Object ref = token_;
        if (ref instanceof String) {
          com.google.protobuf.ByteString b = 
              com.google.protobuf.ByteString.copyFromUtf8(
                  (java.lang.String) ref);
          token_ = b;
          return b;
        } else {
          return (com.google.protobuf.ByteString) ref;
        }
      }
      /**
       * <code>required string token = 1;</code>
       *
       * <pre>
       *token
       * </pre>
       */
      public Builder setToken(
          java.lang.String value) {
        if (value == null) {
    throw new NullPointerException();
  }
  bitField0_ |= 0x00000001;
        token_ = value;
        onChanged();
        return this;
      }
      /**
       * <code>required string token = 1;</code>
       *
       * <pre>
       *token
       * </pre>
       */
      public Builder clearToken() {
        bitField0_ = (bitField0_ & ~0x00000001);
        token_ = getDefaultInstance().getToken();
        onChanged();
        return this;
      }
      /**
       * <code>required string token = 1;</code>
       *
       * <pre>
       *token
       * </pre>
       */
      public Builder setTokenBytes(
          com.google.protobuf.ByteString value) {
        if (value == null) {
    throw new NullPointerException();
  }
  bitField0_ |= 0x00000001;
        token_ = value;
        onChanged();
        return this;
      }

      // @@protoc_insertion_point(builder_scope:com.sojoys.chess.game.protobuf.request.EnterGame)
    }

    static {
      defaultInstance = new EnterGame(true);
      defaultInstance.initFields();
    }

    // @@protoc_insertion_point(class_scope:com.sojoys.chess.game.protobuf.request.EnterGame)
  }

  public interface HeartbeatOrBuilder
      extends com.google.protobuf.MessageOrBuilder {
  }
  /**
   * Protobuf type {@code com.sojoys.chess.game.protobuf.request.Heartbeat}
   *
   * <pre>
   * 258 心跳
   * </pre>
   */
  public static final class Heartbeat extends
      com.google.protobuf.GeneratedMessage
      implements HeartbeatOrBuilder {
    // Use Heartbeat.newBuilder() to construct.
    private Heartbeat(com.google.protobuf.GeneratedMessage.Builder<?> builder) {
      super(builder);
      this.unknownFields = builder.getUnknownFields();
    }
    private Heartbeat(boolean noInit) { this.unknownFields = com.google.protobuf.UnknownFieldSet.getDefaultInstance(); }

    private static final Heartbeat defaultInstance;
    public static Heartbeat getDefaultInstance() {
      return defaultInstance;
    }

    public Heartbeat getDefaultInstanceForType() {
      return defaultInstance;
    }

    private final com.google.protobuf.UnknownFieldSet unknownFields;
    @java.lang.Override
    public final com.google.protobuf.UnknownFieldSet
        getUnknownFields() {
      return this.unknownFields;
    }
    private Heartbeat(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      initFields();
      com.google.protobuf.UnknownFieldSet.Builder unknownFields =
          com.google.protobuf.UnknownFieldSet.newBuilder();
      try {
        boolean done = false;
        while (!done) {
          int tag = input.readTag();
          switch (tag) {
            case 0:
              done = true;
              break;
            default: {
              if (!parseUnknownField(input, unknownFields,
                                     extensionRegistry, tag)) {
                done = true;
              }
              break;
            }
          }
        }
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.setUnfinishedMessage(this);
      } catch (java.io.IOException e) {
        throw new com.google.protobuf.InvalidProtocolBufferException(
            e.getMessage()).setUnfinishedMessage(this);
      } finally {
        this.unknownFields = unknownFields.build();
        makeExtensionsImmutable();
      }
    }
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return com.sojoys.chess.game.protobuf.request.Req_LoginMsg.internal_static_com_sojoys_chess_game_protobuf_request_Heartbeat_descriptor;
    }

    protected com.google.protobuf.GeneratedMessage.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return com.sojoys.chess.game.protobuf.request.Req_LoginMsg.internal_static_com_sojoys_chess_game_protobuf_request_Heartbeat_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              com.sojoys.chess.game.protobuf.request.Req_LoginMsg.Heartbeat.class, com.sojoys.chess.game.protobuf.request.Req_LoginMsg.Heartbeat.Builder.class);
    }

    public static com.google.protobuf.Parser<Heartbeat> PARSER =
        new com.google.protobuf.AbstractParser<Heartbeat>() {
      public Heartbeat parsePartialFrom(
          com.google.protobuf.CodedInputStream input,
          com.google.protobuf.ExtensionRegistryLite extensionRegistry)
          throws com.google.protobuf.InvalidProtocolBufferException {
        return new Heartbeat(input, extensionRegistry);
      }
    };

    @java.lang.Override
    public com.google.protobuf.Parser<Heartbeat> getParserForType() {
      return PARSER;
    }

    private void initFields() {
    }
    private byte memoizedIsInitialized = -1;
    public final boolean isInitialized() {
      byte isInitialized = memoizedIsInitialized;
      if (isInitialized != -1) return isInitialized == 1;

      memoizedIsInitialized = 1;
      return true;
    }

    public void writeTo(com.google.protobuf.CodedOutputStream output)
                        throws java.io.IOException {
      getSerializedSize();
      getUnknownFields().writeTo(output);
    }

    private int memoizedSerializedSize = -1;
    public int getSerializedSize() {
      int size = memoizedSerializedSize;
      if (size != -1) return size;

      size = 0;
      size += getUnknownFields().getSerializedSize();
      memoizedSerializedSize = size;
      return size;
    }

    private static final long serialVersionUID = 0L;
    @java.lang.Override
    protected java.lang.Object writeReplace()
        throws java.io.ObjectStreamException {
      return super.writeReplace();
    }

    public static com.sojoys.chess.game.protobuf.request.Req_LoginMsg.Heartbeat parseFrom(
        com.google.protobuf.ByteString data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static com.sojoys.chess.game.protobuf.request.Req_LoginMsg.Heartbeat parseFrom(
        com.google.protobuf.ByteString data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static com.sojoys.chess.game.protobuf.request.Req_LoginMsg.Heartbeat parseFrom(byte[] data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static com.sojoys.chess.game.protobuf.request.Req_LoginMsg.Heartbeat parseFrom(
        byte[] data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static com.sojoys.chess.game.protobuf.request.Req_LoginMsg.Heartbeat parseFrom(java.io.InputStream input)
        throws java.io.IOException {
      return PARSER.parseFrom(input);
    }
    public static com.sojoys.chess.game.protobuf.request.Req_LoginMsg.Heartbeat parseFrom(
        java.io.InputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return PARSER.parseFrom(input, extensionRegistry);
    }
    public static com.sojoys.chess.game.protobuf.request.Req_LoginMsg.Heartbeat parseDelimitedFrom(java.io.InputStream input)
        throws java.io.IOException {
      return PARSER.parseDelimitedFrom(input);
    }
    public static com.sojoys.chess.game.protobuf.request.Req_LoginMsg.Heartbeat parseDelimitedFrom(
        java.io.InputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return PARSER.parseDelimitedFrom(input, extensionRegistry);
    }
    public static com.sojoys.chess.game.protobuf.request.Req_LoginMsg.Heartbeat parseFrom(
        com.google.protobuf.CodedInputStream input)
        throws java.io.IOException {
      return PARSER.parseFrom(input);
    }
    public static com.sojoys.chess.game.protobuf.request.Req_LoginMsg.Heartbeat parseFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return PARSER.parseFrom(input, extensionRegistry);
    }

    public static Builder newBuilder() { return Builder.create(); }
    public Builder newBuilderForType() { return newBuilder(); }
    public static Builder newBuilder(com.sojoys.chess.game.protobuf.request.Req_LoginMsg.Heartbeat prototype) {
      return newBuilder().mergeFrom(prototype);
    }
    public Builder toBuilder() { return newBuilder(this); }

    @java.lang.Override
    protected Builder newBuilderForType(
        com.google.protobuf.GeneratedMessage.BuilderParent parent) {
      Builder builder = new Builder(parent);
      return builder;
    }
    /**
     * Protobuf type {@code com.sojoys.chess.game.protobuf.request.Heartbeat}
     *
     * <pre>
     * 258 心跳
     * </pre>
     */
    public static final class Builder extends
        com.google.protobuf.GeneratedMessage.Builder<Builder>
       implements com.sojoys.chess.game.protobuf.request.Req_LoginMsg.HeartbeatOrBuilder {
      public static final com.google.protobuf.Descriptors.Descriptor
          getDescriptor() {
        return com.sojoys.chess.game.protobuf.request.Req_LoginMsg.internal_static_com_sojoys_chess_game_protobuf_request_Heartbeat_descriptor;
      }

      protected com.google.protobuf.GeneratedMessage.FieldAccessorTable
          internalGetFieldAccessorTable() {
        return com.sojoys.chess.game.protobuf.request.Req_LoginMsg.internal_static_com_sojoys_chess_game_protobuf_request_Heartbeat_fieldAccessorTable
            .ensureFieldAccessorsInitialized(
                com.sojoys.chess.game.protobuf.request.Req_LoginMsg.Heartbeat.class, com.sojoys.chess.game.protobuf.request.Req_LoginMsg.Heartbeat.Builder.class);
      }

      // Construct using com.sojoys.chess.game.protobuf.request.Req_LoginMsg.Heartbeat.newBuilder()
      private Builder() {
        maybeForceBuilderInitialization();
      }

      private Builder(
          com.google.protobuf.GeneratedMessage.BuilderParent parent) {
        super(parent);
        maybeForceBuilderInitialization();
      }
      private void maybeForceBuilderInitialization() {
        if (com.google.protobuf.GeneratedMessage.alwaysUseFieldBuilders) {
        }
      }
      private static Builder create() {
        return new Builder();
      }

      public Builder clear() {
        super.clear();
        return this;
      }

      public Builder clone() {
        return create().mergeFrom(buildPartial());
      }

      public com.google.protobuf.Descriptors.Descriptor
          getDescriptorForType() {
        return com.sojoys.chess.game.protobuf.request.Req_LoginMsg.internal_static_com_sojoys_chess_game_protobuf_request_Heartbeat_descriptor;
      }

      public com.sojoys.chess.game.protobuf.request.Req_LoginMsg.Heartbeat getDefaultInstanceForType() {
        return com.sojoys.chess.game.protobuf.request.Req_LoginMsg.Heartbeat.getDefaultInstance();
      }

      public com.sojoys.chess.game.protobuf.request.Req_LoginMsg.Heartbeat build() {
        com.sojoys.chess.game.protobuf.request.Req_LoginMsg.Heartbeat result = buildPartial();
        if (!result.isInitialized()) {
          throw newUninitializedMessageException(result);
        }
        return result;
      }

      public com.sojoys.chess.game.protobuf.request.Req_LoginMsg.Heartbeat buildPartial() {
        com.sojoys.chess.game.protobuf.request.Req_LoginMsg.Heartbeat result = new com.sojoys.chess.game.protobuf.request.Req_LoginMsg.Heartbeat(this);
        onBuilt();
        return result;
      }

      public Builder mergeFrom(com.google.protobuf.Message other) {
        if (other instanceof com.sojoys.chess.game.protobuf.request.Req_LoginMsg.Heartbeat) {
          return mergeFrom((com.sojoys.chess.game.protobuf.request.Req_LoginMsg.Heartbeat)other);
        } else {
          super.mergeFrom(other);
          return this;
        }
      }

      public Builder mergeFrom(com.sojoys.chess.game.protobuf.request.Req_LoginMsg.Heartbeat other) {
        if (other == com.sojoys.chess.game.protobuf.request.Req_LoginMsg.Heartbeat.getDefaultInstance()) return this;
        this.mergeUnknownFields(other.getUnknownFields());
        return this;
      }

      public final boolean isInitialized() {
        return true;
      }

      public Builder mergeFrom(
          com.google.protobuf.CodedInputStream input,
          com.google.protobuf.ExtensionRegistryLite extensionRegistry)
          throws java.io.IOException {
        com.sojoys.chess.game.protobuf.request.Req_LoginMsg.Heartbeat parsedMessage = null;
        try {
          parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
        } catch (com.google.protobuf.InvalidProtocolBufferException e) {
          parsedMessage = (com.sojoys.chess.game.protobuf.request.Req_LoginMsg.Heartbeat) e.getUnfinishedMessage();
          throw e;
        } finally {
          if (parsedMessage != null) {
            mergeFrom(parsedMessage);
          }
        }
        return this;
      }

      // @@protoc_insertion_point(builder_scope:com.sojoys.chess.game.protobuf.request.Heartbeat)
    }

    static {
      defaultInstance = new Heartbeat(true);
      defaultInstance.initFields();
    }

    // @@protoc_insertion_point(class_scope:com.sojoys.chess.game.protobuf.request.Heartbeat)
  }

  private static com.google.protobuf.Descriptors.Descriptor
    internal_static_com_sojoys_chess_game_protobuf_request_EnterGame_descriptor;
  private static
    com.google.protobuf.GeneratedMessage.FieldAccessorTable
      internal_static_com_sojoys_chess_game_protobuf_request_EnterGame_fieldAccessorTable;
  private static com.google.protobuf.Descriptors.Descriptor
    internal_static_com_sojoys_chess_game_protobuf_request_Heartbeat_descriptor;
  private static
    com.google.protobuf.GeneratedMessage.FieldAccessorTable
      internal_static_com_sojoys_chess_game_protobuf_request_Heartbeat_fieldAccessorTable;

  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    java.lang.String[] descriptorData = {
      "\n\032request/Req_LoginMsg.proto\022&com.sojoys" +
      ".chess.game.protobuf.request\"\032\n\tEnterGam" +
      "e\022\r\n\005token\030\001 \002(\t\"\013\n\tHeartbeatB\016B\014Req_Log" +
      "inMsg"
    };
    com.google.protobuf.Descriptors.FileDescriptor.InternalDescriptorAssigner assigner =
      new com.google.protobuf.Descriptors.FileDescriptor.InternalDescriptorAssigner() {
        public com.google.protobuf.ExtensionRegistry assignDescriptors(
            com.google.protobuf.Descriptors.FileDescriptor root) {
          descriptor = root;
          internal_static_com_sojoys_chess_game_protobuf_request_EnterGame_descriptor =
            getDescriptor().getMessageTypes().get(0);
          internal_static_com_sojoys_chess_game_protobuf_request_EnterGame_fieldAccessorTable = new
            com.google.protobuf.GeneratedMessage.FieldAccessorTable(
              internal_static_com_sojoys_chess_game_protobuf_request_EnterGame_descriptor,
              new java.lang.String[] { "Token", });
          internal_static_com_sojoys_chess_game_protobuf_request_Heartbeat_descriptor =
            getDescriptor().getMessageTypes().get(1);
          internal_static_com_sojoys_chess_game_protobuf_request_Heartbeat_fieldAccessorTable = new
            com.google.protobuf.GeneratedMessage.FieldAccessorTable(
              internal_static_com_sojoys_chess_game_protobuf_request_Heartbeat_descriptor,
              new java.lang.String[] { });
          return null;
        }
      };
    com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
        }, assigner);
  }

  // @@protoc_insertion_point(outer_class_scope)
}
