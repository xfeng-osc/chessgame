package com.sojoys.chess.game.db;

import com.sojoys.chess.game.db.cache.*;
/**
 * 数据存储
 */
public class DbSave {

	public static boolean saveAll() throws Exception{
		// player
		PlayerCache.getInstance().saveAll();
		return true;
	}
	
	public static boolean shutdownAll() throws Exception{
		// player
		PlayerCache.getInstance().shutdown();
		return true;
	}
//自定义内容起始位置
//自定义内容结束位置
}