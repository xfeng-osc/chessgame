package com.sojoys.chess.game.db.cfg;

import java.util.Map;
import com.sojoys.artifact.constant.DataModelKeyEnum;
import com.sojoys.artifact.build.data.base.BaseCfg;
import com.sojoys.artifact.tools.ToolMap;
/**
 * desk
 */
 public class DeskCfg extends BaseCfg<DeskCfg>{
	/**
	 * 数据库表名称
	 */
	public static final String TABLE_NAME = "desk";
	/**
	 * 完整类名称
	 */
	public static final String CLASS_NAME = "com.sojoys.chess.game.db.cfg.DeskCfg"; 
	/**
	 * 字段个数
	 */
	public static final int COLUMN_COUNT= 4;
	/**
	 * 数据库源字段名字
	 */
	public static final String[] SOURCE_COLUMN_NAMES = new String[]{"id","seat","gold","playing_type_id",};
	/**
	 * 拼音字段名字
	 */
	public static final String[] PINGYING_COLUMN_NAMES = new String[]{"id","seat","gold","playing_type_id",};
	/**
	 * HashCode字段名字
	 */
	public static final Integer[] HASHCODE_COLUMN_NAMES = new Integer[]{3355,3526149,3178592,-1496889041,};
	/**
	 * 字段基本类型
	 */
	public static final String[] COLUMN_BASICTYPES = new String[]{"int","int","int","int",};
	/**
	 * 字段引用类型
	 */
	public static final Class<?>[] COLUMN_CLASSTYPES = new Class[]{Integer.class,Integer.class,Integer.class,Integer.class,};
	
	/** id */
	private int id;
	
	/** 座位数 */
	private int seat;
	
	/** 进入最低金币 */
	private int gold;
	
	/** playing_type_id */
	private int playing_type_id;
	
	
	
	public static DeskCfg builder() {
		DeskCfg deskCfg = new DeskCfg();
		return deskCfg;
	}
	
	/** id */
	public int getId() {
		return id;
	}
	
	/** 座位数 */
	public int getSeat() {
		return seat;
	}
	
	/** 进入最低金币 */
	public int getGold() {
		return gold;
	}
	
	/** playing_type_id */
	public int getPlaying_type_id() {
		return playing_type_id;
	}
	
	
	protected Object[] getColumnNames(DataModelKeyEnum e){
		switch (e) {
		case SOURCE:
			return SOURCE_COLUMN_NAMES;
		case PINGYING:
			return PINGYING_COLUMN_NAMES;
		case HASHCODE:
			return HASHCODE_COLUMN_NAMES;
		default:
			return PINGYING_COLUMN_NAMES;
		}
	}
	
	protected String[] getBasicTypes(){
		return COLUMN_BASICTYPES;
	}
	
	protected Class<?>[] getClassTypes(){
		return COLUMN_CLASSTYPES;
	}
	
	protected Object[] getColumeValues(){
		Object[] values = new Object[COLUMN_COUNT];
        values[0] = this.id;
        values[1] = this.seat;
        values[2] = this.gold;
        values[3] = this.playing_type_id;
        return values;
	}
	
	protected DeskCfg createColumeValues(Object[] vals) {
		this.id = (Integer)vals[0];
		this.seat = (Integer)vals[1];
		this.gold = (Integer)vals[2];
		this.playing_type_id = (Integer)vals[3];
		return this;
	}
	
	protected Map<Object, Object> toMap(Object[] keys,Map<Object, Object> map,boolean encode){
        map.put(keys[0], this.id);
        map.put(keys[1], this.seat);
        map.put(keys[2], this.gold);
        map.put(keys[3], this.playing_type_id);
        return map;
    }
	
	protected DeskCfg createForMap(Object[] keys,Map<?, ?> map,boolean decode){
	    this.id = ToolMap.getInt(keys[0], map);
	    this.seat = ToolMap.getInt(keys[1], map);
	    this.gold = ToolMap.getInt(keys[2], map);
	    this.playing_type_id = ToolMap.getInt(keys[3], map);
        return this;
    }
	
	public DeskCfg deepCopy(){
		DeskCfg deskCfg = null;
		try {
			deskCfg = (DeskCfg) super.clone();
			return deskCfg;
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return deskCfg;
	}
}
