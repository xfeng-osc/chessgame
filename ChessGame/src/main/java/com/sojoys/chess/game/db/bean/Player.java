package com.sojoys.chess.game.db.bean;

import com.sojoys.chess.game.db.cache.PlayerCache;
import org.apache.commons.lang3.ArrayUtils;
import java.util.Map;
import com.sojoys.artifact.tools.SK_Plus;
import com.sojoys.artifact.constant.DataModelKeyEnum;
import com.sojoys.artifact.tools.ToolMap;
import com.sojoys.artifact.build.data.base.BasePojo;
import com.sojoys.artifact.tools.ToolCopy;
import com.sojoys.artifact.factory.bytebuff.IByteBuff;
/**
 * player
 */
public class Player extends BasePojo<Player>{
	/**
	 * 数据库表名称
	 */
	public static final String TABLE_NAME = "player";
	/**
	 * 完整类名称
	 */
	public static final String CLASS_NAME = "com.sojoys.chess.game.db.bean.Player"; 
	/**
	 * 数据库源字段名字
	 */
	public static final String[] SOURCE_COLUMN_NAMES = new String[]{"id","uid","name","gold","sycee","stone","createDate","modifiedDate",};
	/**
	 * 拼音字段名字
	 */
	public static final String[] PINGYING_COLUMN_NAMES = new String[]{"id","uid","name","gold","sycee","stone","createDate","modifiedDate",};
	/**
	 * HashCode字段名字
	 */
	public static final Integer[] HASHCODE_COLUMN_NAMES = new Integer[]{3355,115792,3373707,3178592,109907997,109770853,1368729290,-626009577,};
	/**
	 * 字段基本类型
	 */
	public static final String[] COLUMN_BASICTYPES = new String[]{"String","String","String","long","long","long","java.util.Date","java.util.Date",};
	/**
	 * 字段引用类型
	 */
	public static final Class<?>[] COLUMN_CLASSTYPES = new Class[]{String.class,String.class,String.class,Long.class,Long.class,Long.class,java.util.Date.class,java.util.Date.class,};
	/**
	 * 字段个数
	 */
	public static final int COLUMN_COUNT= 8;
	
	/**
	 * 关联表源名称
	 */ 
	public static final String[] FK_SOURCE_TABLE_NAMES = new String[]{};
	
	/**
	 * 关联表拼音名称
	 */
	public static final String[] FK_PINGYING_TABLE_NAMES = new String[]{};
	
	/**
	 * 关联表HashCode名称
	 */ 
	public static final Integer[] FK_HASHCODE_TABLE_NAMES = new Integer[]{};
	
	/**
	 * 关联表所关联字段源名
	 */ 
	public static final String[] FK_SOURCE_COLUMN_NAMES = new String[]{};
	
	/**
	 * 关联表所关联字段拼音名
	 */ 
	public static final String[] FK_PINGYING_COLUMN_NAMES = new String[]{};
	
	/**
	 * 关联表所关联字段hashcode名
	 */ 
	public static final Integer[] FK_HASHCODE_COLUMN_NAMES = new Integer[]{};
	
	/**
	 * 索引名称
	 */
	public static final String[] TABLE_INDEX_NAMES = new String[]{"id","uid",};
	
	/**
	 * 索引值
	 */
	private String[] indexValues = new String[2];
	
	/** id */
	private String id;
	
	/** 用户id */
	private String uid;
	
	/** 名称 */
	private String name;
	
	/** 金币 */
	private long gold;
	
	/** 元宝 */
	private long sycee;
	
	/** 钻石 */
	private long stone;
	
	/** createDate */
	private java.util.Date createDate;
	
	/** modifiedDate */
	private java.util.Date modifiedDate;
	
	
	public static Player builder() {
		Player player = new Player();
		return player;
	}
	
	public Player() {
		super();
	}
	
	public Player(String id, String uid, String name, long gold, long sycee, long stone, java.util.Date createDate, java.util.Date modifiedDate) {
		super();
		this.id = id;
		this.uid = uid;
		this.name = name;
		this.gold = gold;
		this.sycee = sycee;
		this.stone = stone;
		this.createDate = createDate;
		this.modifiedDate = modifiedDate;
	}
	
	/** id */
	public String getId() {
		return id;
	}
	
	
	/** id */
	public void setId(String id) {
		this.id = id;
		addUpdateColumn(SOURCE_COLUMN_NAMES[0],id);
	}
	
	/** 用户id */
	public String getUid() {
		return uid;
	}
	
	
	/** 用户id */
	public void setUid(String uid) {
		this.uid = uid;
		addUpdateColumn(SOURCE_COLUMN_NAMES[1],uid);
	}
	
	/** 名称 */
	public String getName() {
		return name;
	}
	
	
	/** 名称 */
	public void setName(String name) {
		this.name = name;
		addUpdateColumn(SOURCE_COLUMN_NAMES[2],name);
	}
	
	/** 金币 */
	public long getGold() {
		return gold;
	}
	
	
	/** 金币 */
	public void setGold(long gold) {
		this.gold = gold;
		addUpdateColumn(SOURCE_COLUMN_NAMES[3],gold);
	}
	
	/** 元宝 */
	public long getSycee() {
		return sycee;
	}
	
	
	/** 元宝 */
	public void setSycee(long sycee) {
		this.sycee = sycee;
		addUpdateColumn(SOURCE_COLUMN_NAMES[4],sycee);
	}
	
	/** 钻石 */
	public long getStone() {
		return stone;
	}
	
	
	/** 钻石 */
	public void setStone(long stone) {
		this.stone = stone;
		addUpdateColumn(SOURCE_COLUMN_NAMES[5],stone);
	}
	
	/** createDate */
	public java.util.Date getCreateDate() {
		return createDate;
	}
	
	
	/** createDate */
	public void setCreateDate(java.util.Date createDate) {
		this.createDate = createDate;
		addUpdateColumn(SOURCE_COLUMN_NAMES[6],createDate);
	}
	
	/** modifiedDate */
	public java.util.Date getModifiedDate() {
		return modifiedDate;
	}
	
	
	/** modifiedDate */
	public void setModifiedDate(java.util.Date modifiedDate) {
		this.modifiedDate = modifiedDate;
		addUpdateColumn(SOURCE_COLUMN_NAMES[7],modifiedDate);
	}
	
	
	protected Object[] getColumnNames(DataModelKeyEnum e){
		switch (e) {
		case SOURCE:
			return SOURCE_COLUMN_NAMES;
		case PINGYING:
			return PINGYING_COLUMN_NAMES;
		case HASHCODE:
			return HASHCODE_COLUMN_NAMES;
		default:
			return PINGYING_COLUMN_NAMES;
		}
	}
	
	protected Object[] getFKTableNames(DataModelKeyEnum e){
		switch (e) {
		case SOURCE:
			return FK_SOURCE_TABLE_NAMES;
		case PINGYING:
			return FK_PINGYING_TABLE_NAMES;
		case HASHCODE:
			return FK_HASHCODE_TABLE_NAMES;
		default:
			return FK_PINGYING_TABLE_NAMES;
		}
	}
	
	protected Object[] getFKColumnNames(DataModelKeyEnum e){
		switch (e) {
		case SOURCE:
			return FK_SOURCE_COLUMN_NAMES;
		case PINGYING:
			return FK_PINGYING_COLUMN_NAMES;
		case HASHCODE:
			return FK_HASHCODE_COLUMN_NAMES;
		default:
			return FK_PINGYING_TABLE_NAMES;
		}
	}
	
	protected String[] getBasicTypes(){
		return COLUMN_BASICTYPES;
	}
	
	protected Class<?>[] getClassTypes(){
		return COLUMN_CLASSTYPES;
	}
	
	protected int getColumnCount(){
		return COLUMN_COUNT;
	}
	
	protected Object[] getColumeValues(){
		Object[] values = new Object[COLUMN_COUNT];
        values[0] = this.id;
        values[1] = this.uid;
        values[2] = this.name;
        values[3] = this.gold;
        values[4] = this.sycee;
        values[5] = this.stone;
        values[6] = this.createDate;
        values[7] = this.modifiedDate;
        return values;
	}
	
	public Player deepCopy(){
		Player player = null;
		try {
			player = (Player) super.clone();
			player.setUpdateColumn(ToolCopy.copyObject(this.updateColumn));
		    player.setCreateDate(ToolCopy.copyObject(this.createDate));
		    player.setModifiedDate(ToolCopy.copyObject(this.modifiedDate));
		    // 克隆索引
		    player.indexValues = ArrayUtils.clone(indexValues);
			return player;
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return player;
	}
	
	protected Map<Object, Object> toMap(Object[] keys,Map<Object, Object> map,boolean encode){
        map.put(keys[0], this.id);
        map.put(keys[1], this.uid);
        map.put(keys[2], this.name);
        map.put(keys[3], this.gold);
        map.put(keys[4], this.sycee);
        map.put(keys[5], this.stone);
        map.put(keys[6], this.createDate);
        map.put(keys[7], this.modifiedDate);
        return map;
    }
    
    protected Player createForMap(Object[] keys,Map<?, ?> map,boolean decode){
	    this.id = ToolMap.getString(keys[0], map);
	    this.uid = ToolMap.getString(keys[1], map);
	    this.name = ToolMap.getString(keys[2], map);
	    this.gold = ToolMap.getLong(keys[3], map);
	    this.sycee = ToolMap.getLong(keys[4], map);
	    this.stone = ToolMap.getLong(keys[5], map);
	    this.createDate = ToolMap.getDate(keys[6], map);
	    this.modifiedDate = ToolMap.getDate(keys[7], map);
        return this;
    }
    
    protected void writeBytes(IByteBuff byteBuf){
	    byteBuf.writeString(this.id);
	    byteBuf.writeString(this.uid);
	    byteBuf.writeString(this.name);
	    byteBuf.writeLong(this.gold);
	    byteBuf.writeLong(this.sycee);
	    byteBuf.writeLong(this.stone);
	    byteBuf.writeDate(this.createDate);
	    byteBuf.writeDate(this.modifiedDate);
    }
	
	protected Player readBytes(IByteBuff byteBuf){
	    this.id = byteBuf.readString(null);
	    this.uid = byteBuf.readString(null);
	    this.name = byteBuf.readString(null);
	    this.gold = byteBuf.readLong(null);
	    this.sycee = byteBuf.readLong(null);
	    this.stone = byteBuf.readLong(null);
	    this.createDate = byteBuf.readDate(null);
	    this.modifiedDate = byteBuf.readDate(null);
	    return this;
	}
    
    
    protected Map<?,?> exportFKTableMap(Object[] keys,Map<Object, Object> map,DataModelKeyEnum e,boolean encode){
        return map;
    }
    
    protected boolean importFKTableMap(Object[] keys,Map<Object, Object> map,DataModelKeyEnum e,boolean decode){
		return true;
    }
    
	
    /** 级联删除(延迟入库) */
    public boolean deleteAndSon(){
    	return false;
    }
    
    /** 级联删除(及时入库) */
    public boolean deleteAndSonAndFlush(){
    	return false;
    }
    
    /** 延迟插入数据库 */
    public Player insert(){
    	return PlayerCache.getInstance().insert(this);
    }
    /** 延迟更新数据库 */
    public Player update(){
    	return PlayerCache.getInstance().update(this);
    }
    /** 延迟删除数据库 */
    public boolean delete(){
    	return PlayerCache.getInstance().delete(this);
    }
    /** 即时插入数据库 */
    public Player insertAndFlush(){
    	return PlayerCache.getInstance().insertAndFlush(this);
    }
    /** 即时更新数据库 */
    public Player updateAndFlush(){
    	return PlayerCache.getInstance().updateAndFlush(this);
    }
    /** 即时删除数据库 */
    public boolean deleteAndFlush(){
    	return PlayerCache.getInstance().deleteAndFlush(this);
    }
    
    
	
	/**
	 * 同步索引值
	 */
	public Player syncIndexValues(){
		indexValues[0] = SK_Plus.b(getId()).e();
		indexValues[1] = SK_Plus.b(getUid()).e();
		return this;
	}
	/**
	 * 取得索引
	 */
	public String[] getIndexValues(){
		return indexValues;
	}
	
	public Object getPrimaryKey(){
		return getId();
	}
	
	public void setPrimaryKey(Object key){
		setId((String)key);
	}
	
//自定义内容起始位置
//自定义内容结束位置
}