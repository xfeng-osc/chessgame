/*
 * Copyright 2012 The Netty Project
 *
 * The Netty Project licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package com.sojoys.chess.game.netty;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sojoys.artifact.constant.ServerStatusEnum;
import com.sojoys.artifact.core.IServer;
import com.sojoys.artifact.factory.session.ISession;
import com.sojoys.artifact.plugin.netty.message.NettyMessage;
import com.sojoys.chess.game.db.dao.PlayerDaoImpl;
import com.sojoys.chess.game.module.Dispatcher;
import com.sojoys.chess.game.netty.session.NettySessionManager;

import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.Attribute;
import io.netty.util.AttributeKey;

/**
 * Handler implementation for the echo server.
 */
@Sharable
public class NettyHandler extends ChannelInboundHandlerAdapter {
	
	static final AttributeKey<ISession> STATE = AttributeKey.valueOf("session");
	static final Logger log = LoggerFactory.getLogger(PlayerDaoImpl.class);
	
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {
    	if(IServer.SERVER_STATUS != ServerStatusEnum.RUNNING){
    		log.error("Server Status -> {status}",IServer.SERVER_STATUS );
    	}else {
    		if (msg instanceof NettyMessage) {
        		Attribute<ISession> session= ctx.channel().attr(STATE);
        		ISession isession = session.get();
        		if (isession==null) {
        			isession =  NettySessionManager.getInstance().buildSession(ctx.channel());
        			session.set(isession);
    			}
        		Dispatcher.getInstance().disp(isession,(NettyMessage) msg);
    		}
		}
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) {
        ctx.flush();
        System.out.println("channelReadComplete");
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        // Close the connection when an exception is raised.
    	Attribute<ISession> session= ctx.channel().attr(STATE);
		ISession isession = session.get();
		if (isession!=null) {
			
		}
        cause.printStackTrace();
        ctx.close();
        System.out.println("exceptionCaught");
    }
}