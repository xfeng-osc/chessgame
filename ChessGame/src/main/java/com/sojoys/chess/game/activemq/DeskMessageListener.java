package com.sojoys.chess.game.activemq;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.support.converter.MessageConversionException;
import org.springframework.jms.support.converter.MessageConverter;

import com.sojoys.chess.game.db.dao.PlayerDaoImpl;

/**
 * 消息监听
 * 
 * @author win7
 *
 */
public class DeskMessageListener implements MessageListener {
	MessageConverter messageConverter;

	static final Logger log = LoggerFactory.getLogger(PlayerDaoImpl.class);

	public MessageConverter getMessageConverter() {
		return messageConverter;
	}

	public void setMessageConverter(MessageConverter messageConverter) {
		this.messageConverter = messageConverter;
	}

	@Override
	public void onMessage(Message message) {
		ActiveMQMsg msg = null;
		try {
			msg = (ActiveMQMsg) messageConverter.fromMessage(message);
			System.out.println(msg);
		} catch (MessageConversionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}