package com.sojoys.chess.game.config;

import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import com.sojoys.artifact.core.Constants;
import com.sojoys.artifact.core.IConfig;
import com.sojoys.artifact.core.Modules;
import com.sojoys.artifact.core.Plugins;
import com.sojoys.artifact.manager.ContextManager;
import com.sojoys.artifact.manager.PropManager;
import com.sojoys.artifact.manager.ThreadManager;
import com.sojoys.artifact.plugin.dirtyword.DirtyWordsPlugin;
import com.sojoys.artifact.plugin.quartz.QuartzPlugin;
import com.sojoys.chess.game.plugin.CmdPlugin;
import com.sojoys.chess.game.plugin.DBSavePlugin;
import com.sojoys.chess.game.plugin.NettyPlugin;
import com.sojoys.chess.game.plugin.SubmitServerInfoPlugin;

/**
 * @author : DengYing
 * @CreateDate : 2017年4月17日 下午12:18:36
 * @Description ：Please describe this document
 */
public class GameConfig implements IConfig {

	@Override
	public void configConstant(Constants me) {
		// 设置使用的配置文件
		PropManager.use("app.properties");
		// SpringContext初始化
		ContextManager.init("app-init.xml");
	}

	@Override
	public void configModules(Modules me) {
		// 逻辑Module注册
		me.add(new ModuleScan());
	}

	@Override
	public void configPlugin(Plugins me) {
		// 注意：插件先后顺序影响 插件的启动和停止顺序(即为:先启动 先停止)
		
		// 屏蔽字插件
		me.add(new DirtyWordsPlugin());
		
		// 加载配置档插件
//		me.add(new LoadCfgPlugin());
		
		// 添加定时器插件
		me.add(new QuartzPlugin());
		
		// 向登陆服务器上传服务器信息插件
		me.add(new SubmitServerInfoPlugin());
		
		// Netty服务插件
		me.add(new NettyPlugin());
		
		// 数据定时存储插件
		me.add(new DBSavePlugin());
		
		me.add(new CmdPlugin());
	}

	@Override
	public void afterJFinalStart() {
		System.out.println("=========== Server Start Succeed ===========");
	}

	@Override
	public void beforeJFinalStop() {
		ThreadPoolTaskExecutor task = ThreadManager.getInstance().getThreadPoolTaskExecutor("taskExecutor");
		task.shutdown();
		System.out.println("=========== Server Stop Succeed ===========");
	}

	

}
