package com.sojoys.chess.game;

import com.sojoys.artifact.build.net.NetWork_Generate;

public class BuildNetModel {
	static final String PATH = "src/main/java/com/sojoys/chess/game/module";
	static final String PKG = "com.sojoys.chess.game.module";
	public static void main(String[] args) {
		try {
			NetWork_Generate.run(PATH,PKG, NetWorkServert.class,NetWork_Generate.getConfiguration());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
