package com.sojoys.chess.game.job;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.sojoys.chess.game.db.DbSave;

/**
 * @author : DengYing
 * @CreateDate : 2017年4月29日 上午12:00:38
 * @Description ：Please describe this document
 */
public class DBSaveJob implements Job {

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		try {
			DbSave.saveAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
