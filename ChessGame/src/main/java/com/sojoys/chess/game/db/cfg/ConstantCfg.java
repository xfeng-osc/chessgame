package com.sojoys.chess.game.db.cfg;

import java.util.Map;
import com.sojoys.artifact.constant.DataModelKeyEnum;
import com.sojoys.artifact.build.data.base.BaseCfg;
import com.sojoys.artifact.tools.ToolMap;
/**
 * constant
 */
 public class ConstantCfg extends BaseCfg<ConstantCfg>{
	/**
	 * 数据库表名称
	 */
	public static final String TABLE_NAME = "constant";
	/**
	 * 完整类名称
	 */
	public static final String CLASS_NAME = "com.sojoys.chess.game.db.cfg.ConstantCfg"; 
	/**
	 * 字段个数
	 */
	public static final int COLUMN_COUNT= 2;
	/**
	 * 数据库源字段名字
	 */
	public static final String[] SOURCE_COLUMN_NAMES = new String[]{"id","context",};
	/**
	 * 拼音字段名字
	 */
	public static final String[] PINGYING_COLUMN_NAMES = new String[]{"id","context",};
	/**
	 * HashCode字段名字
	 */
	public static final Integer[] HASHCODE_COLUMN_NAMES = new Integer[]{3355,951530927,};
	/**
	 * 字段基本类型
	 */
	public static final String[] COLUMN_BASICTYPES = new String[]{"String","String",};
	/**
	 * 字段引用类型
	 */
	public static final Class<?>[] COLUMN_CLASSTYPES = new Class[]{String.class,String.class,};
	
	/** id */
	private String id;
	
	/** 内容 */
	private String context;
	
	
	
	public static ConstantCfg builder() {
		ConstantCfg constantCfg = new ConstantCfg();
		return constantCfg;
	}
	
	/** id */
	public String getId() {
		return id;
	}
	
	/** 内容 */
	public String getContext() {
		return context;
	}
	
	
	protected Object[] getColumnNames(DataModelKeyEnum e){
		switch (e) {
		case SOURCE:
			return SOURCE_COLUMN_NAMES;
		case PINGYING:
			return PINGYING_COLUMN_NAMES;
		case HASHCODE:
			return HASHCODE_COLUMN_NAMES;
		default:
			return PINGYING_COLUMN_NAMES;
		}
	}
	
	protected String[] getBasicTypes(){
		return COLUMN_BASICTYPES;
	}
	
	protected Class<?>[] getClassTypes(){
		return COLUMN_CLASSTYPES;
	}
	
	protected Object[] getColumeValues(){
		Object[] values = new Object[COLUMN_COUNT];
        values[0] = this.id;
        values[1] = this.context;
        return values;
	}
	
	protected ConstantCfg createColumeValues(Object[] vals) {
		this.id = (String)vals[0];
		this.context = (String)vals[1];
		return this;
	}
	
	protected Map<Object, Object> toMap(Object[] keys,Map<Object, Object> map,boolean encode){
        map.put(keys[0], this.id);
        map.put(keys[1], this.context);
        return map;
    }
	
	protected ConstantCfg createForMap(Object[] keys,Map<?, ?> map,boolean decode){
	    this.id = ToolMap.getString(keys[0], map);
	    this.context = ToolMap.getString(keys[1], map);
        return this;
    }
	
	public ConstantCfg deepCopy(){
		ConstantCfg constantCfg = null;
		try {
			constantCfg = (ConstantCfg) super.clone();
			return constantCfg;
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return constantCfg;
	}
}
