package com.sojoys.chess.game.db.cfg.cache;

import java.util.ArrayList;
import java.sql.Connection;
import com.sojoys.artifact.tools.ToolList;
import java.util.List;
import com.sojoys.artifact.constant.DataModelKeyEnum;
import com.sojoys.chess.game.db.cfg.Playing_typeCfg;
import com.sojoys.artifact.build.data.base.BaseCfgCache;
import com.sojoys.artifact.tools.SK_PageCount;
/**
 * playing_type
 */
public class Playing_typeCfgCache extends BaseCfgCache<Playing_typeCfg,Integer>{

	private static final Playing_typeCfgCache INSTANCE = new Playing_typeCfgCache();

	public static Playing_typeCfgCache getInstance() {  
        return INSTANCE;  
    } 
		
	public List<Playing_typeCfg> loadDatabase(Connection conn) {
		return loadDatabase(conn, Playing_typeCfg.TABLE_NAME);
	}
	
	
	
	/**
	 * 加载缓存
	 */
	protected void loadCache(Playing_typeCfg playing_typeCfg){
		cache.put(playing_typeCfg.getId(),playing_typeCfg);
	}
	
	/**
	 * 加载缓存
	 */
	protected void clearCache(Playing_typeCfg playing_typeCfg){
		cache.remove(playing_typeCfg.getId());
		
	}
	
	protected String getTable(){
		return Playing_typeCfg.TABLE_NAME;
	}
	
	protected Playing_typeCfg builderCfg(){
		return Playing_typeCfg.builder();
	}
	
	protected String[] getBasicTypes(){
		return Playing_typeCfg.COLUMN_BASICTYPES;
	}
	
	protected Object[] getColumnNames(DataModelKeyEnum e){
		switch (e) {
		case SOURCE:
			return Playing_typeCfg.SOURCE_COLUMN_NAMES;
		case PINGYING:
			return Playing_typeCfg.PINGYING_COLUMN_NAMES;
		case HASHCODE:
			return Playing_typeCfg.HASHCODE_COLUMN_NAMES;
		default:
			return Playing_typeCfg.PINGYING_COLUMN_NAMES;
		}
	}
	
	
	/**
	 * 根据( id ) 查询
	 */
	public Playing_typeCfg getById(int id){
		return cache.get(id);
	}
	
	
	public List<Playing_typeCfg> getAll(){
		return new ArrayList<Playing_typeCfg>(cache.values());
	}
	
	public List<Playing_typeCfg> getAll(int page,int size,SK_PageCount pageCount){
		List<Playing_typeCfg> playing_typeCfgs = getAll();
		playing_typeCfgs = ToolList.getPage(playing_typeCfgs, page, size, pageCount);
		return playing_typeCfgs;
	}
//自定义内容起始位置
//自定义内容结束位置
}