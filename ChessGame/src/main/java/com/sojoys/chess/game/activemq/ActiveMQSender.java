package com.sojoys.chess.game.activemq;

import org.springframework.jms.core.JmsTemplate;

/**
 * @author : DengYing
 * @CreateDate : 2017年4月26日 下午4:19:03
 * @Description ：Please describe this document
 */
public class ActiveMQSender {
	
	private static final ActiveMQSender INSTANCE = new ActiveMQSender();
	
	public static ActiveMQSender getInstance() {
		return INSTANCE;
	}
	
	JmsTemplate jmsTemplate;
	
	
	public void setJmsTemplate(JmsTemplate jmsTemplate) {
		this.jmsTemplate = jmsTemplate;
	}

	/**
	 * 发送消息
	 * @param destination
	 * @param msg
	 */
	public void sendMsg(String destination,ActiveMQMsg msg) {
		jmsTemplate.convertAndSend("", msg);
	}
	
	public void sendMsg(ActiveMQMsg msg) {
		jmsTemplate.convertAndSend(msg);
	}
}
