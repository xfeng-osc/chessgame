package com.sojoys.chess.game.plugin;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import com.sojoys.artifact.core.IPlugin;
import com.sojoys.artifact.manager.PropManager;
import com.sojoys.artifact.manager.ThreadManager;
import com.sojoys.artifact.plugin.quartz.QuartzPlugin;
import com.sojoys.chess.game.constant.AppConstant;
import com.sojoys.chess.game.echo.EchoClient;
import com.sojoys.chess.game.echo.OnStartupListener;
import com.sojoys.chess.game.job.SubmitServerInfoJob;

/**
 * @author : DengYing
 * @CreateDate : 2017年4月17日 下午4:03:13
 * @Description ：上传服务器信息插件
 */
public class SubmitServerInfoPlugin implements IPlugin {
	static final Logger log = LoggerFactory.getLogger(SubmitServerInfoPlugin.class);
	
	EchoClient client = null;
	@Override
	public boolean start() {
		String host = PropManager.get(AppConstant.LOGIN_TCP_HOST);
		int port = PropManager.getInt(AppConstant.LOGIN_TCP_PORT);
		client = new EchoClient(host, port, false,new OnStartupListener() {
			@Override
			public void onCompletion(boolean bool,EchoClient client) {
				String groupName = "SubmitServerInfo";
				String jobName = "client";
				if (bool) {
					if (client != null) {
						Map<String, Object> param = new HashMap<>();
						param.put("client", client);
						QuartzPlugin.addJob(groupName, jobName, new Date(), "0/30 * * * * ?", SubmitServerInfoJob.class,param);
					}
				}else {
					if (client != null) {
						Map<String, Object> param = new HashMap<>();
						param.put("client", client);
						QuartzPlugin.removeJob(groupName, jobName);
					}
				}
			}
		});
		
		ThreadPoolTaskExecutor task = ThreadManager.getInstance().getThreadPoolTaskExecutor("taskExecutor");
		task.execute(client);
		return true;
	}

	@Override
	public boolean stop() {
		client.shutdown();
		return true;
	}
}
