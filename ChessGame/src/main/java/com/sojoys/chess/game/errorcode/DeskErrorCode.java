package com.sojoys.chess.game.errorcode;

public class DeskErrorCode {
	private static final int CODE = 2000;
	/**
	 * 找不到配置
	 */
	public static final int PERMISSION_DENIED = CODE + 1;
}
