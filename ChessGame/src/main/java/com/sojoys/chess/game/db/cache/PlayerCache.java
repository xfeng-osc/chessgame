package com.sojoys.chess.game.db.cache;

import java.util.concurrent.ConcurrentHashMap;
import com.sojoys.chess.game.db.bean.Player;
import com.sojoys.artifact.build.data.base.BaseCache;
import com.sojoys.artifact.tools.SK_Plus;
import com.sojoys.artifact.build.data.base.BaseDao;
import com.sojoys.chess.game.db.dao.PlayerDaoImpl;
/**
 * player
 */
public class PlayerCache extends BaseCache<String, Player ,PlayerDaoImpl>{
	private PlayerCache(BaseDao<Player> baseDao) {
		super(baseDao);
	}
	private static final PlayerCache INSTANCE = new PlayerCache(PlayerDaoImpl.getInstance());
      
    
    public static PlayerCache getInstance() {  
        return INSTANCE;  
    }  

	
	
	/** 唯一索引缓存 */
	final ConcurrentHashMap<String, String> uidCache = new ConcurrentHashMap<String, String>();
	
	// 被释放对象
	
	
	/**
	 * 根据( id ) 查询
	 */
	public Player getById(String id){
		Player player = cache.get(id);
		if(player==null){
			player = getBaseDao().getById(id);
			if(player!=null){
				player = addCache(player,null);
			}
		}
		return player;
	}
	
	/**
	 * 根据( uid ) 查询
	 */
	public Player getByUid(String uid){
		Player player = null;
		String key = uidCache.get(SK_Plus.b(uid).e());
		if(key!=null){
			player = getById(key);	
		} 
		if(player==null){
			player =  getBaseDao().getByUid(uid);
			if(player!=null){
				player = addCache(player,null);
			}
		}
		return player;
	}
	
	/**
	 * 添加缓存
	 * @param indexName 为null时不添加[聚集索引缓存] 
	 * 即:只添加指定的聚集索引缓存，避免加载单个对象时生成聚集索引缓存
	 * @return
	 */
	protected Player addCache(Player player,String indexName){
		// 如果对象在待删除中
		if(containsDeleteSave(player)){
			return null;
		} 
		//避免重复加载(主要是避免：从数据库加载上来的为SAVE的对象替换缓存对象)
		if(!cache.containsKey(player.getId()))
			cache.put(player.getId(),player);
		else{
			player = cache.get(player.getId());
			//清除历史缓存
			clearCache(player,false);
			
			cache.put(player.getId(),player);
		}
		
		// 同步索引 * 必须是要在清空缓存后才能同步
		player.syncIndexValues();
		
		uidCache.put(player.getIndexValues()[1],player.getId());
		
		
		return player;
	}
	
	
	
	/**
	 * 清空缓存 
	 */
	protected void clearCache(Player player,boolean isRelease){
		cache.remove(player.getId());
		
		uidCache.remove(player.getIndexValues()[1]);
		
		//清空缓存记录
		if(isRelease){
		}
	}
    // ******************************** 级联操作开始 ********************************
    
    /**
	 * 级联加载缓存
	 */
	public void deepLoadCache(Player player){
		
		addCache(player,null);
		
	}
	
	/**
	 * 级联清除缓存
	 */
	public void deepClearCache(Player player){
	
		clearCache(player,true);
		
	}
	 
	/**
	 * 级联删除
	 */
	public void deepDelete(Player player){
	
		delete(player,false);
		
	}
	
	 // ******************************** 级联操作结束 ********************************
	
//自定义内容起始位置
//自定义内容结束位置
}