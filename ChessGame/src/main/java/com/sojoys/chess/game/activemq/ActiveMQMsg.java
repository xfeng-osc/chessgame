package com.sojoys.chess.game.activemq;

import java.io.Serializable;
import java.util.Date;

import com.sojoys.artifact.tools.ToolDate;

/**
 * @author : DengYing
 * @CreateDate : 2017年4月26日 下午4:07:44
 * @Description ：Please describe this document
 */
public class ActiveMQMsg implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7260947885611165371L;

	private int commandId;


	public ActiveMQMsg(int commandId) {
		super();
		this.commandId = commandId;
	}

	public int getCommandId() {
		return commandId;
	}

	public void setCommandId(int commandId) {
		this.commandId = commandId;
	}
	
	@Override
	public String toString() {
		return "ActiveMQMsg [commandId=" + commandId + "]";
	}

	public static void main(String[] args) {
		System.out.println(ToolDate.formatDate("2017-4-26 19:00:00",ToolDate.fmt_yyyy_MM_dd_HH_mm_ss).getTime());
		
		System.out.println(new Date(1493142602872l));
	}
}
