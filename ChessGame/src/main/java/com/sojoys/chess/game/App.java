package com.sojoys.chess.game;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.sojoys.artifact.manager.ContextManager;
import com.sojoys.artifact.manager.JsonManager;
import com.sojoys.artifact.manager.PropManager;
import com.sojoys.artifact.plugin.netty.message.NettyMessage;
import com.sojoys.artifact.plugin.quartz.QuartzPlugin;
import com.sojoys.artifact.tools.ToolMap;
import com.sojoys.artifact.tools.ToolSimpleHttp;
import com.sojoys.chess.game.activemq.ActiveMQMsg;
import com.sojoys.chess.game.activemq.ActiveMQSender;
import com.sojoys.chess.game.echo.EchoClient;
import com.sojoys.chess.game.echo.OnStartupListener;
import com.sojoys.chess.game.job.SubmitServerInfoJob;
import com.sojoys.chess.game.protobuf.request.Req_LoginMsg;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) {
//		EchoClient echoClient = null;
//		try {
//			echoClient = new EchoClient().connect("127.0.0.1", 8007, false);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		if (echoClient != null) {
//			EchoMsg.GameServerRegisterRequest.Builder request = EchoMsg.GameServerRegisterRequest.newBuilder();
//			request.setAddress("192.168.7.3:8800");
//			request.setOnline(1);
//			request.setCpuRatio(ToolOS.getOscpuRatio());
//			request.setMemoryRatio((int) ToolOS.getJvmFreeMemory());
//			NettyMessage msg = new NettyMessage();
//			msg.setCommandId((short) 257);
//			msg.setBody(request.build().toByteArray());
//			echoClient.sendMsg(msg);
//		}
		
//		short NOTIFY_REWARD_BY_TYPE = 1 << 8 | 1;
//		System.out.println(NOTIFY_REWARD_BY_TYPE);
//		System.out.println(NOTIFY_REWARD_BY_TYPE >> 8);
//		System.out.println(NOTIFY_REWARD_BY_TYPE ^ 45 << 8);
		
//		PropManager.use("app.properties");
//
//		ContextManager.init("app-init.xml");
//		
//		try {
//			ConfigLoad.writeAllExcel("C:\\Users\\win7\\Desktop\\config\\bbb.xls");
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
		
//		String json = ToolSimpleHttp.sendGet("http://119.29.24.161:8080/ChessLogin/login", "code=SandKing&channel=NAN&name=NAN&deviceId=NAN&deviceName=NAN&deviceToken=NAN&osName=NAN&osVersion=NAN&sdkName=NAN&sdkVersion=NAN&mcc=NAN");
//		final Map data1 = JsonManager.getJson().parse(json, Map.class);
//		final Map data = (Map) data1.get("data");
//		
//		String string = ToolMap.getString("address", data);
//		String[] address = string.split(":");
//		EchoClient client = new EchoClient(address[0], Integer.valueOf(address[1]), false,new OnStartupListener() {
//			@Override
//			public void onCompletion(boolean bool,EchoClient client) {
//				if (bool) {
//					if (client != null) {
//						Req_LoginMsg.EnterGame.Builder b = Req_LoginMsg.EnterGame.newBuilder();
//						b.setToken(ToolMap.getString("token", data));
//						NettyMessage msg = new NettyMessage();
//						msg.setCommandId((short) 257);
//						msg.setBody(b.build().toByteArray());
//						client.sendMessage(msg);
//					}
//				}else {
//					if (client != null) {
//						
//					}
//				}
//			}
//		});
//		
//		new Thread(client).start();
		
		PropManager.use("app.properties");
		// SpringContext初始化
		ContextManager.init("app-init.xml");
		ActiveMQSender.getInstance().sendMsg(new ActiveMQMsg(999));			
	}
}
