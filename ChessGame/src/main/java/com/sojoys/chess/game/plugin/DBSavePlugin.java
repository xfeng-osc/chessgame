package com.sojoys.chess.game.plugin;

import java.util.Date;

import com.sojoys.artifact.core.IPlugin;
import com.sojoys.artifact.plugin.quartz.QuartzPlugin;
import com.sojoys.chess.game.db.DbSave;
import com.sojoys.chess.game.job.DBSaveJob;

/**
 * @author : DengYing
 * @CreateDate : 2017年4月28日 上午12:04:53
 * @Description ：数据存储插件
 */
public class DBSavePlugin implements IPlugin {

	@Override
	public boolean start() {
		QuartzPlugin.addJob("DBSavePlugin", "DBSavePlugin", new Date(), "0/10 * * * * ? ", DBSaveJob.class,null);
		return true;
	}

	@Override
	public boolean stop() {
		try {
			return DbSave.shutdownAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

}
