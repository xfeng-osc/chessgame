package com.sojoys.chess.game.db.cfg.cache;

import com.sojoys.artifact.tools.SetMap;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.ArrayList;
import java.sql.Connection;
import com.sojoys.artifact.tools.ToolList;
import java.util.List;
import com.sojoys.artifact.tools.SK_Plus;
import com.sojoys.artifact.constant.DataModelKeyEnum;
import com.sojoys.chess.game.db.cfg.DeskCfg;
import com.sojoys.artifact.build.data.base.BaseCfgCache;
import com.sojoys.artifact.tools.SK_PageCount;
/**
 * desk
 */
public class DeskCfgCache extends BaseCfgCache<DeskCfg,Integer>{

	private static final DeskCfgCache INSTANCE = new DeskCfgCache();

	public static DeskCfgCache getInstance() {  
        return INSTANCE;  
    } 
		
	public List<DeskCfg> loadDatabase(Connection conn) {
		return loadDatabase(conn, DeskCfg.TABLE_NAME);
	}
	
	
	/** 聚集索引缓存 */
	final SetMap<String, Integer> playing_type_idCache = new SetMap<String, Integer>();
	
	
	/**
	 * 加载缓存
	 */
	protected void loadCache(DeskCfg deskCfg){
		cache.put(deskCfg.getId(),deskCfg);
		playing_type_idCache.getIfAbsent(SK_Plus.b(deskCfg.getPlaying_type_id()).e()).add(deskCfg.getId());
	}
	
	/**
	 * 加载缓存
	 */
	protected void clearCache(DeskCfg deskCfg){
		cache.remove(deskCfg.getId());
		
		
		playing_type_idCache.setRemove(SK_Plus.b(deskCfg.getPlaying_type_id()).e(),deskCfg.getId());
	}
	
	protected String getTable(){
		return DeskCfg.TABLE_NAME;
	}
	
	protected DeskCfg builderCfg(){
		return DeskCfg.builder();
	}
	
	protected String[] getBasicTypes(){
		return DeskCfg.COLUMN_BASICTYPES;
	}
	
	protected Object[] getColumnNames(DataModelKeyEnum e){
		switch (e) {
		case SOURCE:
			return DeskCfg.SOURCE_COLUMN_NAMES;
		case PINGYING:
			return DeskCfg.PINGYING_COLUMN_NAMES;
		case HASHCODE:
			return DeskCfg.HASHCODE_COLUMN_NAMES;
		default:
			return DeskCfg.PINGYING_COLUMN_NAMES;
		}
	}
	
	
	/**
	 * 根据( id ) 查询
	 */
	public DeskCfg getById(int id){
		return cache.get(id);
	}
	
	/**
	 * 根据( playing_type_id ) 查询
	 */
	public List<DeskCfg> getByPlaying_type_id(int playing_type_id){
		List<DeskCfg> deskCfgs = new ArrayList<DeskCfg>();
		final String key = SK_Plus.b(playing_type_id).e();
		ConcurrentSkipListSet<Integer> keys = playing_type_idCache.get(key);
		if(keys != null){
			DeskCfg deskCfg = null;
			for (Integer k : keys) {
				deskCfg = getById(k);
				if (deskCfg == null) continue;
					deskCfgs.add(deskCfg);
			}
		}
		return deskCfgs;
	}
	
	public List<DeskCfg> getByPagePlaying_type_id(int playing_type_id,int page,int size,SK_PageCount pageCount){
		List<DeskCfg> deskCfgs = getByPlaying_type_id(playing_type_id);
		deskCfgs = ToolList.getPage(deskCfgs, page, size, pageCount);
		return deskCfgs;
	}
	
	public List<DeskCfg> getAll(){
		return new ArrayList<DeskCfg>(cache.values());
	}
	
	public List<DeskCfg> getAll(int page,int size,SK_PageCount pageCount){
		List<DeskCfg> deskCfgs = getAll();
		deskCfgs = ToolList.getPage(deskCfgs, page, size, pageCount);
		return deskCfgs;
	}
//自定义内容起始位置
//自定义内容结束位置
}