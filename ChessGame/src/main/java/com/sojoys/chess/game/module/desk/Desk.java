package com.sojoys.chess.game.module.desk;

import com.sojoys.artifact.exception.LogicModelException;
import com.sojoys.artifact.factory.session.ISession;
import com.sojoys.artifact.core.IModule;
/** 
 * 牌桌
 */
public interface Desk extends IModule {
	byte classCode = 2;
	
	/** 进入牌桌 [code = 513] */
	byte[] enterDesk(ISession session, com.sojoys.chess.game.protobuf.request.Req_DeskMsg.EnterDesk request) throws LogicModelException;
	
	/** 换桌 [code = 514] */
	byte[] switchDesk(ISession session) throws LogicModelException;
	
	/** 退出 [code = 515] */
	byte[] exitDesk(ISession session, com.sojoys.chess.game.protobuf.request.Req_DeskMsg.ExitDesk request) throws LogicModelException;
	
}