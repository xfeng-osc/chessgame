package com.sojoys.chess.game.db.cfg.cache;

import java.util.ArrayList;
import java.sql.Connection;
import com.sojoys.artifact.tools.ToolList;
import com.sojoys.chess.game.db.cfg.ConstantCfg;
import java.util.List;
import com.sojoys.artifact.constant.DataModelKeyEnum;
import com.sojoys.artifact.build.data.base.BaseCfgCache;
import com.sojoys.artifact.tools.SK_PageCount;
/**
 * constant
 */
public class ConstantCfgCache extends BaseCfgCache<ConstantCfg,String>{

	private static final ConstantCfgCache INSTANCE = new ConstantCfgCache();

	public static ConstantCfgCache getInstance() {  
        return INSTANCE;  
    } 
		
	public List<ConstantCfg> loadDatabase(Connection conn) {
		return loadDatabase(conn, ConstantCfg.TABLE_NAME);
	}
	
	
	
	/**
	 * 加载缓存
	 */
	protected void loadCache(ConstantCfg constantCfg){
		cache.put(constantCfg.getId(),constantCfg);
	}
	
	/**
	 * 加载缓存
	 */
	protected void clearCache(ConstantCfg constantCfg){
		cache.remove(constantCfg.getId());
		
	}
	
	protected String getTable(){
		return ConstantCfg.TABLE_NAME;
	}
	
	protected ConstantCfg builderCfg(){
		return ConstantCfg.builder();
	}
	
	protected String[] getBasicTypes(){
		return ConstantCfg.COLUMN_BASICTYPES;
	}
	
	protected Object[] getColumnNames(DataModelKeyEnum e){
		switch (e) {
		case SOURCE:
			return ConstantCfg.SOURCE_COLUMN_NAMES;
		case PINGYING:
			return ConstantCfg.PINGYING_COLUMN_NAMES;
		case HASHCODE:
			return ConstantCfg.HASHCODE_COLUMN_NAMES;
		default:
			return ConstantCfg.PINGYING_COLUMN_NAMES;
		}
	}
	
	
	/**
	 * 根据( id ) 查询
	 */
	public ConstantCfg getById(String id){
		return cache.get(id);
	}
	
	
	public List<ConstantCfg> getAll(){
		return new ArrayList<ConstantCfg>(cache.values());
	}
	
	public List<ConstantCfg> getAll(int page,int size,SK_PageCount pageCount){
		List<ConstantCfg> constantCfgs = getAll();
		constantCfgs = ToolList.getPage(constantCfgs, page, size, pageCount);
		return constantCfgs;
	}
//自定义内容起始位置
//自定义内容结束位置
}