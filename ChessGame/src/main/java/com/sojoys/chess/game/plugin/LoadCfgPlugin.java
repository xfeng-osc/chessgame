package com.sojoys.chess.game.plugin;

import com.sojoys.artifact.core.IPlugin;
import com.sojoys.chess.game.db.ConfigLoad;

/**
 * @author : DengYing
 * @CreateDate : 2017年4月21日 下午5:49:12
 * @Description ：Please describe this document
 */
public class LoadCfgPlugin implements IPlugin {

	@Override
	public boolean start() {
		try {
			return ConfigLoad.loadAllConfig("");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean stop() {
		return true;
	}

}
