package com.sojoys.chess.game.constant;

/**
 * @author : DengYing
 * @CreateDate : 2017年4月30日 上午12:31:44
 * @Description ：Please describe this document
 */
public interface AppConstant {
	/**
	 * 登陆服务器地址
	 */
	String LOGIN_SERVER_ADDRESS = "login.server.address";

	/**
	 * handler包位置
	 */
	String MODULE_SCAN_PACKAGE = "module.scan.package";
	
	
	String CONFIG_DEVMODE = "config.devMode";
	
	String NETTY_HOST = "netty.host";
	
	String NETTY_PORT = "netty.port";
	
	String LOGIN_TCP_HOST = "login.tcp.host";
	
	String LOGIN_TCP_PORT = "login.tcp.port";
	
	String REMOTE_ADDRESS = "remote.address";
}
