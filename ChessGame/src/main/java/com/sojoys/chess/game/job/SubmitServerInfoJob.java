package com.sojoys.chess.game.job;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sojoys.artifact.manager.PropManager;
import com.sojoys.artifact.plugin.netty.message.NettyMessage;
import com.sojoys.artifact.tools.ToolOS;
import com.sojoys.chess.game.constant.AppConstant;
import com.sojoys.chess.game.echo.EchoClient;
import com.sojoys.chess.game.plugin.SubmitServerInfoPlugin;
import com.sojoys.chess.game.protobuf.EchoMsg;

public class SubmitServerInfoJob implements Job {
	static final Logger log = LoggerFactory.getLogger(SubmitServerInfoPlugin.class);

	@Override
	public void execute(JobExecutionContext ctx) throws JobExecutionException {
		String host = PropManager.get(AppConstant.REMOTE_ADDRESS);
		int port = PropManager.getInt(AppConstant.NETTY_PORT);
		EchoClient client = (EchoClient) ctx.getMergedJobDataMap().get("client");
		EchoMsg.GameServerRegister.Builder request = EchoMsg.GameServerRegister.newBuilder();
		request.setAddress(host+":"+port);
		request.setOnline(1);
		request.setCpuRatio(ToolOS.getOscpuRatio());
		request.setMemoryRatio((int) ToolOS.getJvmFreeMemory());
		NettyMessage msg = new NettyMessage();
		msg.setCommandId((short) 257);
		msg.setBody(request.build().toByteArray());
		client.sendMessage(msg);
		log.info("Submit ServerInfo");
	}

}