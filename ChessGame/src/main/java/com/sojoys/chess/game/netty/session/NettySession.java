package com.sojoys.chess.game.netty.session;

import java.net.SocketAddress;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.sojoys.artifact.factory.session.ISession;

import io.netty.channel.Channel;

public class NettySession implements ISession {

	protected Channel channel;
	
	private Map<String, Object> attribute = new ConcurrentHashMap<>();

	public NettySession(Channel channel) {
		super();
		this.channel = channel;
	}

	// ----------------------------------------
	@Override
	public Object getId() {
		return channel.id().asShortText();
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T getAttribute(Object key) {
		return (T) attribute.get(key);
	}

	@Override
	public void setAttribute(String key, Object value) {
		attribute.put(key, value);
	}

	@Override
	public void removeAttribute(Object key) {
		// TODO Auto-generated method stub

	}

	@Override
	public SocketAddress localAddress() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SocketAddress remoteAddress() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getIoChannel() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setIoChannel(Object ioChannel) {
		// TODO Auto-generated method stub

	}

	@Override
	public int[] getEncryptionKeys() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int[] getDecryptionKeys() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void close() {
		channel.close();
	}

	@Override
	public void sendMessage(Object msg) {
		channel.writeAndFlush(msg);
	}
}
