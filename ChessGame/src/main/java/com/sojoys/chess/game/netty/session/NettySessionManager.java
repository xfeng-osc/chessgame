package com.sojoys.chess.game.netty.session;

import com.sojoys.artifact.factory.session.ISession;

import io.netty.channel.Channel;

public class NettySessionManager {
	private static final NettySessionManager INSTANCE = new NettySessionManager();

	public static NettySessionManager getInstance() {
		return INSTANCE;
	}
	
	public ISession buildSession(Channel channel){
		ISession session = new NettySession(channel);
		return session;
	}
}
