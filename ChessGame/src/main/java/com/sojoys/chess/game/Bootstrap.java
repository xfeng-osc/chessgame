package com.sojoys.chess.game;

import com.sojoys.artifact.core.IServer;
import com.sojoys.chess.game.config.GameConfig;

public class Bootstrap {
	public static void main(String[] args) {
		IServer.setIConfig(new GameConfig());
		IServer.start();
	}
}
