package com.sojoys.chess.game;

import java.io.File;
import java.io.IOException;

import com.sojoys.artifact.build.data.SK_Generate;
import com.sojoys.artifact.build.data.decoder.SK_FileType;
import com.sojoys.artifact.build.data.decoder.SK_MysqlType;
import com.sojoys.artifact.manager.ContextManager;
import com.sojoys.artifact.manager.DataSourceManager;

import jxl.Workbook;
import jxl.read.biff.BiffException;

/**
 * @author : DengYing
 * @CreateDate : 2017年4月10日 下午9:38:48
 * @Description ：Please describe this document
 */
public class BuildDataModel {
	// 生成数据层的代码路径
	static final String PATH = "src/main/java/com/sojoys/chess/game/db/";
	static final String PKG = "com.sojoys.chess.game.db";

	public static void main(String[] args) {
		// 初始配置文件
		ContextManager.init("app-init.xml");
		/**
		 * SK_Config.getDesignDataSource() : 需要生成的数据库
		 * SK_Generate.getConfiguration() : 使用的模板文件路径 false :
		 * 是否是生成配置库(我把固定不变的表称之为配置库) SK_Generate.CACHE_REDIS_MYSQL : 生成的模式
		 * (占时有3种生成方式)
		 */
		// SK_Generate.run(SK_Config.getDesignDataSource(),PATH,
		// SK_Generate.getConfiguration(), false,SK_Generate.CACHE_MYSQL);
		boolean fileData = false;
		if (!fileData) {
			SK_Generate.run(DataSourceManager.getInstance().getBuildSources(), PATH,PKG,
					SK_Generate.getConfiguration(), new SK_MysqlType(), false, SK_Generate.CACHE_MYSQL);

			// SK_Generate.run(SK_Config.getDesignDataSource(),PATH,
			// SK_Generate.getConfiguration(),new SK_MysqlType(),
			// false,SK_Generate.CACHE_MYSQL);
			// SK_Generate.run(SK_Config.getConfigDataSource(),PATH,
			// SK_Generate.getConfiguration(),new SK_MysqlType(),
			// true,SK_Generate.CACHE_MYSQL);
		} else {
			String dataFilePath = System.getProperty("user.dir") + File.separator + "DataFile" + File.separator
					+ "design.xls";
			Workbook workbook = null;
			try {
				workbook = Workbook.getWorkbook(new File(dataFilePath));
				SK_Generate.run(workbook, PATH,PKG, SK_Generate.getConfiguration(), new SK_FileType(), false,
						SK_Generate.CACHE_REDIS, SK_Generate.DATABASE_MYSQL);
			} catch (BiffException | IOException e) {
				e.printStackTrace();
			} finally {
				if (workbook != null) {
					workbook.close();
				}
			}
		}
	}
}
