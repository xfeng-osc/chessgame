package com.sojoys.chess.game.module.login;

import com.sojoys.artifact.exception.LogicModelException;
import com.sojoys.artifact.factory.session.ISession;
import com.sojoys.artifact.core.IModule;
/** 
 * 登陆
 */
public interface Login extends IModule {
	byte classCode = 1;
	
	/** 进入游戏 [code = 257] */
	byte[] enterGame(ISession session, com.sojoys.chess.game.protobuf.request.Req_LoginMsg.EnterGame request) throws LogicModelException;
	
	/** 心跳 [code = 258] */
	byte[] heartbeat(ISession session, com.sojoys.chess.game.protobuf.request.Req_LoginMsg.Heartbeat request) throws LogicModelException;
	
}