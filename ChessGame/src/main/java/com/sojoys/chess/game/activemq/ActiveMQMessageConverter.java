package com.sojoys.chess.game.activemq;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.Session;

import org.apache.activemq.command.ActiveMQObjectMessage;
import org.springframework.jms.support.converter.MessageConversionException;
import org.springframework.jms.support.converter.MessageConverter;

/**
 * @author : DengYing
 * @CreateDate : 2017年4月26日 下午3:53:47
 * @Description ：ActiveMQ 传输序列化 & 反序列化 (采用的java序列化)
 */
public class ActiveMQMessageConverter implements MessageConverter {

	@Override
	public Object fromMessage(Message message) throws JMSException, MessageConversionException {
		if (message instanceof ObjectMessage) {
			ObjectMessage objectMessage = (ObjectMessage) message;
			try {
				ActiveMQMsg msg = (ActiveMQMsg) objectMessage.getObject();
				return msg;
			} catch (Exception e) {

			}
		}
		return null;
	}

	@Override
	public Message toMessage(Object object, Session session) throws JMSException, MessageConversionException {
		if (object instanceof ActiveMQMsg) {
			ObjectMessage msg = session.createObjectMessage();
			msg.setObject((ActiveMQMsg) object);
			return msg;
		}
		return null;
	}

}
