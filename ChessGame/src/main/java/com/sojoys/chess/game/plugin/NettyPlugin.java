package com.sojoys.chess.game.plugin;

import com.sojoys.artifact.core.IPlugin;
import com.sojoys.artifact.manager.PropManager;
import com.sojoys.chess.game.constant.AppConstant;
import com.sojoys.chess.game.netty.NettyServer;

/**
 * @author : DengYing
 * @CreateDate : 2017年4月21日 下午5:32:18
 * @Description ：Please describe this document
 */
public class NettyPlugin implements IPlugin {
	NettyServer nettyServer = null;
	@Override
	public boolean start() {
		nettyServer = new NettyServer();
		String host = PropManager.get(AppConstant.NETTY_HOST);
		int port = PropManager.getInt(AppConstant.NETTY_PORT);
		try {
			nettyServer.bind(host, port, false);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean stop() {
		if (nettyServer!=null) {
			nettyServer.shutdown();
			return true;
		}
		return false;
	}

}
