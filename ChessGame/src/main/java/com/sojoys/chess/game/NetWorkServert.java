package com.sojoys.chess.game;

import com.sojoys.artifact.annotation.IModule;
import com.sojoys.artifact.exception.LogicModelException;
import com.sojoys.chess.game.protobuf.request.Req_DeskMsg;
import com.sojoys.chess.game.protobuf.request.Req_LoginMsg;

/**
 * 不能是抽象类或接口
 * 
 * @author SandKing
 * 
 */
public class NetWorkServert {

	@IModule(describe = "登陆", code = 1)
	class Login {
		@IModule(describe = "进入游戏", code = 1, proto = true)
		public void enterGame(Req_LoginMsg.EnterGame request) throws LogicModelException {
		};

		@IModule(describe = "心跳", code = 2, proto = true)
		public void heartbeat(Req_LoginMsg.Heartbeat request) throws LogicModelException {
		};
	}

	@IModule(describe = "牌桌", code = 2)
	class Desk {
		@IModule(describe = "进入牌桌", code = 1, proto = true)
		public void enterDesk(Req_DeskMsg.EnterDesk request) throws LogicModelException {
		};

		@IModule(describe = "换桌", code = 2, proto = true)
		public void switchDesk() throws LogicModelException {
		};

		@IModule(describe = "退出", code = 3, proto = true)
		public void exitDesk(Req_DeskMsg.ExitDesk request) throws LogicModelException {
		};
	}
}
