package com.sojoys.chess.game.db.dao;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.MapHandler;
import javax.sql.DataSource;
import com.sojoys.artifact.manager.DataSourceManager;
import java.util.Map;
import com.xiaoleilu.hutool.log.LogFactory;
import java.util.List;
import com.sojoys.artifact.tools.SK_Plus;
import org.apache.commons.dbutils.handlers.MapListHandler;
import com.sojoys.artifact.build.data.base.BaseDao;
import com.sojoys.artifact.tools.SK_Collections;
import com.sojoys.chess.game.db.bean.Player;
import java.sql.Connection;
import com.xiaoleilu.hutool.log.Log;
import org.apache.commons.dbutils.DbUtils;
import com.sojoys.artifact.constant.DataModelKeyEnum;
/**
 * player
 */
public class PlayerDaoImpl implements BaseDao<Player>{
	static Log log = LogFactory.get(PlayerDaoImpl.class);
	/**
	 * 私有化无参构造方法
	 */
	private PlayerDaoImpl() {}
	  
    private static final PlayerDaoImpl INSTANCE = new PlayerDaoImpl();  
    
    public static PlayerDaoImpl getInstance() {  
        return INSTANCE;  
    }  
	
	public Player insert(Player player){
		Connection conn = DataSourceManager.getInstance().getMainConnection();
		return insert(player,conn);
	}
	
	public Player insert(Player player,Connection conn){
		return insert(player,conn,Player.TABLE_NAME);
	}
	
	public Player insert(Player player,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return insert(player,conn);
		} catch (Exception e) {
			log.error(e);
			return null;
		}
	}
	
	public Player insert(Player player,String tableName){
		Connection conn = DataSourceManager.getInstance().getMainConnection();
		return insert(player,conn,tableName);
	}
	
	public Player insert(Player player,Connection conn,String tableName){
		
		QueryRunner run = new QueryRunner();
		String sql = "INSERT INTO " +tableName+ " (id,uid,name,gold,sycee,stone,createDate,modifiedDate) VALUES (?,?,?,?,?,?,?,?)";
		try {
			int i = run.update(conn,sql,player.getId(),player.getUid(),player.getName(),player.getGold(),player.getSycee(),player.getStone(),player.getCreateDate(),player.getModifiedDate());
			return i > 0 ? player : null;
		} catch (Exception e) {
			log.error(e);
			return null;
		} finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				log.error(e1);
				return null;
			}
		}
	}
	
	public Player insert(Player player,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return insert(player,conn,tableName);
		} catch (Exception e) {
			log.error(e);
			return null;
		}		
	}
	
	public int[] insertBatch(List<Player> players){
		Connection conn = DataSourceManager.getInstance().getMainConnection();
		return insertBatch(players,conn);
	}
	
	public int[] insertBatch(List<Player> players,Connection conn){
		return insertBatch(players,conn,Player.TABLE_NAME);
	}
	
	public int[] insertBatch(List<Player> players,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return insertBatch(players,conn);
		} catch (Exception e) {
			log.error(e);
			return null;
		}
	}
	
	public int[] insertBatch(List<Player> players,String tableName){
		Connection conn = DataSourceManager.getInstance().getMainConnection();
		return insertBatch(players,conn,tableName);
	}
	
	public int[] insertBatch(List<Player> players,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "INSERT INTO " +tableName+ " (id,uid,name,gold,sycee,stone,createDate,modifiedDate) VALUES (?,?,?,?,?,?,?,?)";
		try {
			int columnSize = 8;
			int size = players.size();
			Object[][] params = new Object[size][columnSize];
			for (int i = 0; i < size; i++) {
				params[i][0] =players.get(i).getId();
				params[i][1] =players.get(i).getUid();
				params[i][2] =players.get(i).getName();
				params[i][3] =players.get(i).getGold();
				params[i][4] =players.get(i).getSycee();
				params[i][5] =players.get(i).getStone();
				params[i][6] =players.get(i).getCreateDate();
				params[i][7] =players.get(i).getModifiedDate();
			}
			int[] is = run.batch(conn,sql,params);
			return is.length > 0 ? is : new int[]{};
		} catch (Exception e) {
			log.error(e);
			return new int[]{};
		} finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				log.error(e1);
				return null;
			}
		}
	}
	
	public int[] insertBatch(List<Player> players,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return insertBatch(players,conn,tableName);
		} catch (Exception e) {
			log.error(e);
			return null;
		}		
	}
	
	public Player update(Player player){
		Connection conn = DataSourceManager.getInstance().getMainConnection();
		return update(player,conn);
	}
	
	public Player update(Player player,Connection conn){
		return update(player,conn,Player.TABLE_NAME);
	}
	
	public Player update(Player player,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return update(player,conn);
		} catch (Exception e) {
			log.error(e);
			return null;
		}
	}
	
	public Player update(Player player,String tableName){
		Connection conn = DataSourceManager.getInstance().getMainConnection();
		return update(player,conn,tableName);
	}
	
	public Player update(Player player,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		StringBuffer sb = new StringBuffer();
		Map<String, Object> updateColumns = player.getUpdateColumn();
		int columnSize = updateColumns.size();
		if (updateColumns.isEmpty()) {
			return player;
		}
		sb.append("UPDATE ");
		sb.append(tableName);
		sb.append(" SET ");
		Object[] values = new Object[(columnSize + 1)];
		int i = 0;
		for (Map.Entry<String, Object> updateColumn : updateColumns.entrySet()) {
			String key = updateColumn.getKey();
			values[i] = updateColumn.getValue();
			i++;
			sb.append(key);
			sb.append("=");
			sb.append("?");
			if (i < columnSize) {
				sb.append(",");
			}
		}
		sb.append(" WHERE ");
		sb.append("id");
		sb.append(" = ?");
		values[columnSize] = player.getId();
		String sql = sb.toString();
		try {
			i = run.update(conn, sql, values);			
			return i == 1 ? player : null;
		} catch (Exception e) {
			log.error(e);
			return null;
		}finally {
			try{
				// player.clearUpdateColumn();
				DbUtils.close(conn);
			}catch (Exception e1) {
				log.error(e1);
				return null;
			}
		}
	}
	
	public Player update(Player player,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return update(player,conn,tableName);
		} catch (Exception e) {
			log.error(e);
			return null;
		}
	}
	
	public int[] updateBatch(List<Player> players){
		Connection conn = DataSourceManager.getInstance().getMainConnection();
		return updateBatch(players,conn);
	}
	
	public int[] updateBatch(List<Player> players,Connection conn){
		return updateBatch(players,conn,Player.TABLE_NAME);
	}
	
	public int[] updateBatch(List<Player> players,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return updateBatch(players,conn);
		} catch (Exception e) {
			log.error(e);
			return null;
		}
	}
	
	public int[] updateBatch(List<Player> players,String tableName){
		Connection conn = DataSourceManager.getInstance().getMainConnection();
		return updateBatch(players,conn,tableName);
	}
	
	public int[] updateBatch(List<Player> players,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "UPDATE " +tableName+ " SET id = ?,uid = ?,name = ?,gold = ?,sycee = ?,stone = ?,createDate = ?,modifiedDate = ? WHERE id = ?";
		try {
			int columnSize = 8;
			int size = players.size();
			Object[][] params = new Object[size][columnSize + 1];
			for (int i = 0; i < size; i++) {
				params[i][0] =players.get(i).getId();
				params[i][1] =players.get(i).getUid();
				params[i][2] =players.get(i).getName();
				params[i][3] =players.get(i).getGold();
				params[i][4] =players.get(i).getSycee();
				params[i][5] =players.get(i).getStone();
				params[i][6] =players.get(i).getCreateDate();
				params[i][7] =players.get(i).getModifiedDate();
				params[i][columnSize] =players.get(i).getId();
			}
			int[] is = run.batch(conn,sql,params);
			return is.length > 0 ? is : new int[]{};
		} catch (Exception e) {
			log.error(e);
			return new int[]{};
		} finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				log.error(e1);
				return null;
			}
		}
	}
	
	public int[] updateBatch(List<Player> players,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return updateBatch(players,conn,tableName);
		} catch (Exception e) {
			log.error(e);
			return null;
		}		
	}
	
	public boolean delete(Player player){
		Connection conn = DataSourceManager.getInstance().getMainConnection();
		return delete(player,conn);
	}
	
	public boolean delete(Player player,Connection conn){
		return delete(player,conn,Player.TABLE_NAME);
	}
	
	public boolean delete(Player player,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return delete(player,conn);
		} catch (Exception e) {
			log.error(e);
			return false;
		}
	}
	
	public boolean delete(Player player,String tableName){
		Connection conn = DataSourceManager.getInstance().getMainConnection();
		return delete(player,conn,tableName);
	}
	
	public boolean delete(Player player,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "DELETE FROM " + tableName + " WHERE id = ?";
		try {
			int i = run.update(conn,sql, player.getId());
			return i > 0 ? true : false;
		} catch (Exception e) {
			log.error(e);
			return false;
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				log.error(e1);
				return false;
			}
		}
	}
	
	public boolean delete(Player player,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return delete(player,conn,tableName);
		} catch (Exception e) {
			log.error(e);
			return false;
		}
	}
	
	
	public boolean deleteBatch(List<Player> players){
		Connection conn = DataSourceManager.getInstance().getMainConnection();
		return deleteBatch(players,conn);
	}
	
	public boolean deleteBatch(List<Player> players,Connection conn){
		return deleteBatch(players,conn,Player.TABLE_NAME);
	}
	
	public boolean deleteBatch(List<Player> players,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return deleteBatch(players,conn);
		} catch (Exception e) {
			log.error(e);
			return false;
		}
	}
	
	public boolean deleteBatch(List<Player> players,String tableName){
		Connection conn = DataSourceManager.getInstance().getMainConnection();
		return deleteBatch(players,conn,tableName);
	}
	
	public boolean deleteBatch(List<Player> players,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "DELETE FROM " + tableName + " WHERE id = ?";
		try {
			int size = players.size();
			Object[][] params = new Object[size][1];
			for (int i = 0; i < size; i++) {
				params[i][0] = players.get(i).getId();
			}
			int[] is = run.batch(conn,sql,params);
			return is.length > 0;
		} catch (Exception e) {
			log.error(e);
			return false;
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				log.error(e1);
				return false;
			}
		}
	}
	
	public boolean deleteBatch(List<Player> players,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return deleteBatch(players,conn,tableName);
		} catch (Exception e) {
			log.error(e);
			return false;
		}
	}

	/**
	 * 根据( id ) 查询
	 */
	public Player getById(String id){
		Connection conn = DataSourceManager.getInstance().getMainConnection();
		return getById(id, conn);
	}
	
	public Player getById(String id,String tableName){
		Connection conn = DataSourceManager.getInstance().getMainConnection();
		return getById(id, conn,tableName);
	}
	public List<Player> getByIdIn(String[] ids){
		Connection conn = DataSourceManager.getInstance().getMainConnection();
		return getByIdIn(ids, conn);
	}
	
	public List<Player> getByIdIn(String[] ids,String tableName){
		Connection conn = DataSourceManager.getInstance().getMainConnection();
		return getByIdIn(ids, conn,tableName);
	}
	/**
	 * 根据( uid ) 查询
	 */
	public Player getByUid(String uid){
		Connection conn = DataSourceManager.getInstance().getMainConnection();
		return getByUid(uid, conn);
	}
	
	public Player getByUid(String uid,String tableName){
		Connection conn = DataSourceManager.getInstance().getMainConnection();
		return getByUid(uid, conn,tableName);
	}
	
	//Connection
	/**
	 * 根据( id ) 查询
	 */
	public Player getById(String id,Connection conn){
		return getById(id,conn,Player.TABLE_NAME);
	}
	
	public Player getById(String id,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,uid,name,gold,sycee,stone,createDate,modifiedDate FROM " + tableName + " WHERE " + "id = ? ORDER BY id ASC";
		Player player = null; 
		try {
			Map<String, Object> map = run.query(conn,sql, new MapHandler(), id);
			if(map!=null)
				player = Player.builder().createForMap(DataModelKeyEnum.SOURCE, map);
		} catch (Exception e) {
			log.error(e);
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				log.error(e1);
				return null;
			}
		}
		return player;
	}
	public List<Player> getByIdIn(String[] ids,Connection conn){
		return getByIdIn(ids,conn,Player.TABLE_NAME);
	}
	
	public List<Player> getByIdIn(String[] ids,Connection conn,String tableName){
		SK_Plus sp = SK_Plus.newSK_Plus();
		for (int i = 0; i < ids.length; i++) {
			if (i>0) sp.a(",");
			sp.a(ids[i]);
		}
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,uid,name,gold,sycee,stone,createDate,modifiedDate FROM " + tableName + " WHERE " + "id in (%s) ORDER BY id ASC";
		sql = String.format(sql, sp.e());
		List<Player> players = SK_Collections.newArrayList(); 
		try {
			List<Map<String,Object>> list = run.query(conn,sql, new MapListHandler());
			for(Map<String,Object> map : list){
				players.add(Player.builder().createForMap(DataModelKeyEnum.SOURCE, map));
			}
		} catch (Exception e) {
			log.error(e);
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				log.error(e1);
				return null;
			}
		}
		return players;
	}
	/**
	 * 根据( uid ) 查询
	 */
	public Player getByUid(String uid,Connection conn){
		return getByUid(uid,conn,Player.TABLE_NAME);
	}
	
	public Player getByUid(String uid,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,uid,name,gold,sycee,stone,createDate,modifiedDate FROM " + tableName + " WHERE " + "uid = ? ORDER BY id ASC";
		Player player = null; 
		try {
			Map<String, Object> map = run.query(conn,sql, new MapHandler(), uid);
			if(map!=null)
				player = Player.builder().createForMap(DataModelKeyEnum.SOURCE, map);
		} catch (Exception e) {
			log.error(e);
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				log.error(e1);
				return null;
			}
		}
		return player;
	}
	
	//DataSource
	/**
	 * 根据( id ) 查询
	 */
	public Player getById(String id,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getById(id, conn);
		} catch (Exception e) {
			log.error(e);
			return null;
		}
	}
	
	public Player getById(String id,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getById(id, conn);
		} catch (Exception e) {
			log.error(e);
			return null;
		}
	}
	public List<Player> getByIdIn(String[] ids,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getByIdIn(ids, conn);
		} catch (Exception e) {
			log.error(e);
			return null;
		}
	}
	
	public List<Player> getByIdIn(String[] ids,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getByIdIn(ids, conn);
		} catch (Exception e) {
			log.error(e);
			return null;
		}
	}
	/**
	 * 根据( uid ) 查询
	 */
	public Player getByUid(String uid,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getByUid(uid, conn);
		} catch (Exception e) {
			log.error(e);
			return null;
		}
	}
	
	public Player getByUid(String uid,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getByUid(uid, conn);
		} catch (Exception e) {
			log.error(e);
			return null;
		}
	}
	
	
	public List<Player> getAll(){
		Connection conn = DataSourceManager.getInstance().getMainConnection();
		return getAll(conn);
	}
	
	public List<Player> getAll(Connection conn){
		return getAll(conn,Player.TABLE_NAME);
	}
	
	public List<Player> getAll(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getAll(conn);
		} catch (Exception e) {
			log.error(e);
			return null;
		}
	}
	
	public List<Player> getAll(String tableName){
		Connection conn = DataSourceManager.getInstance().getMainConnection();
		return getAll(conn,tableName);
	}
	
	public List<Player> getAll(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,uid,name,gold,sycee,stone,createDate,modifiedDate FROM " + tableName + " ORDER BY id ASC";
		List<Player> players = SK_Collections.newArrayList(); 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			for(Map<String,Object> map : list){
				players.add(Player.builder().createForMap(DataModelKeyEnum.SOURCE, map));
			}
		} catch (Exception e) {
			log.error(e);
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				log.error(e1);
				return null;
			}
		}
		return players;
	}
	
	public List<Player> getAll(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getAll(conn,tableName);
		} catch (Exception e) {
			log.error(e);
			return null;
		}		
	}
	
	public List<Player> getAllPage(int page,int pageSize){
		Connection conn = DataSourceManager.getInstance().getMainConnection();
		return getAllPage(conn,page,pageSize);
	}
	
	public List<Player> getAllPage(Connection conn,int page,int pageSize){
		return getAllPage(conn,Player.TABLE_NAME,page,pageSize);
	}
	
	public List<Player> getAllPage(DataSource ds,int page,int pageSize){
		try {
			Connection conn = ds.getConnection();
			return getAllPage(conn,page,pageSize);
		} catch (Exception e) {
			log.error(e);
			return null;
		}
	}
	
	public List<Player> getAllPage(String tableName,int page,int pageSize){
		Connection conn = DataSourceManager.getInstance().getMainConnection();
		return getAllPage(conn,tableName,page,pageSize);
	}
	
	public List<Player> getAllPage(Connection conn,String tableName,int page,int pageSize){
		QueryRunner run = new QueryRunner();
		page = ((page-1) * pageSize);
		String sql = "SELECT id,uid,name,gold,sycee,stone,createDate,modifiedDate FROM " + tableName + " ORDER BY id ASC LIMIT " + page + " , " +pageSize;
		List<Player> players = SK_Collections.newArrayList(); 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			for(Map<String,Object> map : list){
				players.add(Player.builder().createForMap(DataModelKeyEnum.SOURCE, map));
			}
		} catch (Exception e) {
			log.error(e);
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				log.error(e1);
				return null;
			}
		}
		return players;
	}
	
	public List<Player> getAllPage(DataSource ds,String tableName,int page,int pageSize){
		try {
			Connection conn = ds.getConnection();
			return getAllPage(conn,tableName,page,pageSize);
		} catch (Exception e) {
			log.error(e);
			return null;
		}		
	}
	
	public boolean truncate(){
		Connection conn = DataSourceManager.getInstance().getMainConnection();
		return truncate(conn);
	}
	
	public boolean truncate(Connection conn){
		return truncate(conn,Player.TABLE_NAME);
	}
	
	public boolean truncate(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return truncate(conn);
		} catch (Exception e) {
			log.error(e);
			return false;
		}
	}
	
	public boolean truncate(String tableName){
		Connection conn = DataSourceManager.getInstance().getMainConnection();
		return truncate(conn,tableName);
	}
	
	public boolean truncate(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "TRUNCATE " + tableName;
		try {
			run.update(conn, sql);
			return true;
		} catch (Exception e) {
			log.error(e);
			return false;
		} finally {
			try {
				DbUtils.close(conn);
			} catch (Exception e1) {
				log.error(e1);
				return false;
			}
		}
	}
	
	public boolean truncate(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return truncate(conn,tableName);
		} catch (Exception e) {
			log.error(e);
			return false;
		}
	}
	
	
	
	//Drop Table
	public boolean drop(){
		Connection conn = DataSourceManager.getInstance().getMainConnection();
		return drop(conn);
	}
	
	public boolean drop(Connection conn){
		return drop(conn,Player.TABLE_NAME);
	}
	
	public boolean drop(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return drop(conn);
		} catch (Exception e) {
			log.error(e);
			return false;
		}
	}
	
	public boolean drop(String tableName){
		Connection conn = DataSourceManager.getInstance().getMainConnection();
		return drop(conn,tableName);
	}
	
	public boolean drop(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "DROP TABLE " + tableName;
		try {
			run.update(conn, sql);
			return true;
		} catch (Exception e) {
			log.error(e);
			return false;
		} finally {
			try {
				DbUtils.close(conn);
			} catch (Exception e1) {
				log.error(e1);
				return false;
			}
		}
	}
	
	public boolean drop(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return drop(conn,tableName);
		} catch (Exception e) {
			log.error(e);
			return false;
		}
	}
	
	
	//create
	public boolean createTable(){
		Connection conn = DataSourceManager.getInstance().getMainConnection();
		return createTable(conn);
	}
	
	public boolean createTable(Connection conn){
		return createTable(conn,Player.TABLE_NAME);
	}
	
	public boolean createTable(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return createTable(conn);
		} catch (Exception e) {
			log.error(e);
			return false;
		}
	}
	
	public boolean createTable(String tableName){
		Connection conn = DataSourceManager.getInstance().getMainConnection();
		return createTable(conn,tableName);
	}
	
	public boolean createTable(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		SK_Plus plus = SK_Plus.b("CREATE TABLE IF NOT EXISTS `", tableName,"` (");
		plus.a("  `id` VARCHAR(45) NOT NULL,");	
		plus.a("  `uid` VARCHAR(45) NOT NULL,");	
		plus.a("  `name` VARCHAR(45) NOT NULL,");	
		plus.a("  `gold` BIGINT(19) NOT NULL,");	
		plus.a("  `sycee` BIGINT(19) NOT NULL,");	
		plus.a("  `stone` BIGINT(19) NOT NULL,");	
		plus.a("  `createDate` DATETIME(19) NOT NULL,");	
		plus.a("  `modifiedDate` DATETIME(19) NOT NULL,");	
		plus.a("  PRIMARY KEY (`id`)");
		plus.a(") ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;");
		String sql = plus.e();
		try {
			run.update(conn, sql);
			return true;
		} catch (Exception e) {
			log.error(e);
			return false;
		} finally {
			try {
				DbUtils.close(conn);
			} catch (Exception e1) {
				log.error(e1);
				return false;
			}
		}
	}
	
	public boolean createTable(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return createTable(conn,tableName);
		} catch (Exception e) {
			log.error(e);
			return false;
		}
	}
//自定义内容起始位置
//自定义内容结束位置
}